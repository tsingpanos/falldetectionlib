package com.example.tsigg.falldetectionlib;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.example.tsigg.falldetectionlib.fsm.FSM;
import com.example.tsigg.falldetectionlib.fsm.InputFSM;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by tsigg on 28/4/2017.
 */
@RunWith(AndroidJUnit4.class)
public class FsmTest {

    @Test
    public void Fsm() throws Exception {
        Context appContext = InstrumentationRegistry.getTargetContext();

        BufferedReader mTimeStream = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/TIME.txt")));
        BufferedReader mAccXStream = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/ACC_X.txt")));
        BufferedReader mAccYStream = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/ACC_Y.txt")));
        BufferedReader mAccZStream = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/ACC_Z.txt")));

        FSM fsm = new FSM(appContext);
        double t, x, y, z;
        ArrayList<Double> tArray = crunchifyCSVtoArrayList(mTimeStream.readLine());
        ArrayList<Double> xArray = crunchifyCSVtoArrayList(mAccXStream.readLine());
        ArrayList<Double> yArray = crunchifyCSVtoArrayList(mAccYStream.readLine());
        ArrayList<Double> zArray = crunchifyCSVtoArrayList(mAccZStream.readLine());

        for (int i=0; i<tArray.size(); i++) {
            t = tArray.get(i);
            x = xArray.get(i);
            y = yArray.get(i);
            z = zArray.get(i);
            InputFSM input = new InputFSM(t, x, y, z);
            fsm.run(input);
        }
    }

    // Utility which converts CSV to ArrayList using Split Operation
    public static ArrayList<Double> crunchifyCSVtoArrayList(String crunchifyCSV) {
        ArrayList<Double> crunchifyResult = new ArrayList<Double>();

        if (crunchifyCSV != null) {
            String[] splitData = crunchifyCSV.split("\\s*,\\s*");
            for (int i = 0; i < splitData.length; i++) {
                if (!(splitData[i] == null) || !(splitData[i].length() == 0)) {
                    String t = splitData[i].trim();
                    crunchifyResult.add(Double.parseDouble(t));
                }
            }
        }

        return crunchifyResult;
    }
}

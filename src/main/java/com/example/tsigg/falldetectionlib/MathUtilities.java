package com.example.tsigg.falldetectionlib;

/**
 * Created by tsigg on 23/10/2016.
 */

public class MathUtilities {

    private MathUtilities() {

    }

    public static double[] diff(double[] a, boolean abs) {
        assert a.length > 0;
        double[] res = new double[a.length-1];
        for (int i=0; i<res.length; i++) {
            if (abs)
                res[i] = Math.abs(a[i + 1] - a[i]);
            else
                res[i] = a[i + 1] - a[i];
        }
        return res;
    }
/*
    public static double sum(double[] a) {
        double sum = 0;
        for (int i=0; i<a.length; i++)
            sum += a[i];
        return sum;
    }
*/
    public static double[] sub(double[] a, double k) {
        double[] s = new double[a.length];
        for (int i=0; i<s.length; i++)
            s[i] = a[i] - k;
        return s;
    }
/*
    public static double max(double[] a) {
        double max = -100;
        for (int i=0; i<a.length; i++)
            if (a[i] > max)
                max = a[i];
        return max;
    }

    public static double min(double[] a) {
        double min = 100;
        for (int i=0; i<a.length; i++)
            if (a[i] < min)
                min = a[i];
        return min;
    }

    public static double mean(double[] a) {
        double mean = sum(a) / a.length;
        return mean;
    }
*/
    public static double[] pow(double[] a, int p) {
        double[] pow = new double[a.length];
        for (int i=0; i<pow.length; i++)
            pow[i] = Math.pow(a[i], p);

        return pow;
    }
/*
    public static double[] toDoubleArray(double[] array) {
        double[] res = new double[array.length];
        for (int i=0; i<array.length; i++) {
            res[i] = (double) array[i];
        }
        return res;
    }
*/
}

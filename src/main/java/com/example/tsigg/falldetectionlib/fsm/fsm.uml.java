/*
@startuml
package fsm {
    class FSM
    class FallDetection
    class WalkDetection
    Enum FallState
    Enum FallEvent
    Enum WalkState
    Enum WalkEvent
    class Input
    class InputBuffer
}

class FSM {
    -mWalkDetectionFSM : WalkDetection
    -mFallDetectionFSM : FallDetection
    +run(input : Input)
}

class FallDetection {
    -{static} MATRIX_FALL : FallState[][]
    -{static} mInstance : FallDetection
    -mBuffer : InputBuffer
    +getInstance() : FallDetection
    +setGravityVector(Double[])
    +getCurrentStateFall() : FallState
    +getNextStateFall() : FallState
    +getEventOccurredFall() : FallEvent
    +isFallDetected() : Boolean
    +getClassificationInput() : InputBuffer
    +getImpactEndTime() : Double
    +getImpactStartTime() : Double
    +getPeakTime() : Double
    +run(Input)
    -findImpactStart(Double[]) : Integer
    -findImpactEnd(Double[]) : Integer
}

class WalkDetection {
    -{static} MATRIX_WALK : WalkState[][]
    -{static} mInstance : WalkDetection
    -mBuffer : InputBuffer
    -mGravityVector : Double[]
    -mWalkSegment : ArrayList
    -mGroupStartTime : Double
    -mGroupEndTime : Double
    -mLastPeakTime : Double
    -mFirstGroup : Boolean
    +getInstance() : WalkDetection
    +setGravityVector(Double[])
    +getCurrentStateWalk() : WalkState
    +getNextStateWalk() : WalkState
    +getEventOccurredWalk() : WalkEvent
    +isWalkDetected() : Boolean
    +run(Input)
    -segmentDurTest() : Double
    -stepLengthTest() : Double
}

class Input {
    +Input(Double, Double, Double, Double)
    +setTime(time : Double)
    +setX(x : Double)
    +setY(y : Double)
    +setZ(z : Double)
    +setM(m : Double)
    +getM() : Double
    +getTime() : Double
    +getX() : Double
    +getY() : Double
    +getZ() : Double
    +toString() : String
}

class InputBuffer {
    -mTime : ArrayList<Double>
    -mXAxis : ArrayList<Double>
    -mYAxis : ArrayList<Double>
    -mZAxis : ArrayList<Double>
    -mMaxSize : Integer
    +InputBuffer(Integer)
    +InputBuffer(Double[], Double[], Double[], Double[], Integer)
    +add(Input) : Integer
    +get(Integer) : Input
    +getTime() : Double[]
    +getXAxis() : Double[]
    +getYAxis() : Double[]
    +getZAxis() : Double[]
    +getMagnitude() : Double[]
    +getValuesAtTime(Double) : Input
    +getTimeRange() : Double[]
    +getXAxisRange() : Double[]
    +getYAxisRange() : Double[]
    +getZAxisRange() : Double[]
    +getMagnitudeRange() : Double[]
    +getValuesAtTimeRange(Double) : Input
    +indexOfTime(Double) : Integer
}

Enum FallState {
    NO_STATE
    SAMPLING
    POST_PEAK
    POST_FALL
    ACTIVITY
    FALL
}
Enum FallEvent {
    NO_EVENT
    EXCEED_PEAK
    BOUNCING_END
    POST_FALL_END
    ACTIVITY_NOT_FOUND
    ACTIVITY_FOUND
}
Enum WalkState {
    NO_STATE
    GROUP_START
    GROUP_END
    STEP_LENGTH_TEST
    SEGMENT_DUR_TEST
    STEP_REGULARITY_TEST
    WALK
}
Enum WalkEvent {
    NO_EVENT
    GROUP_PEAKS_FOUND
    GROUP_END_FOUND
    STEP_LENGTH_OK
    FIRST_GROUP
    STEP_LENGTH_FAIL
    SEGMENT_DUR_OK
    SEGMENT_DUR_FAIL
    STEP_REGULARITY_OK
    STEP_REGULARITY_FAIL
}
@enduml

 */
package com.example.tsigg.falldetectionlib.fsm;

import org.apache.commons.math3.stat.StatUtils;

import java.util.ArrayList;

import static com.example.tsigg.falldetectionlib.fsm.WalkDetectionFSM.WalkEvent.EVENT_FIRST_GROUP;
import static com.example.tsigg.falldetectionlib.fsm.WalkDetectionFSM.WalkEvent.EVENT_GROUP_END_FOUND;
import static com.example.tsigg.falldetectionlib.fsm.WalkDetectionFSM.WalkEvent.EVENT_GROUP_PEAKS_FOUND;
import static com.example.tsigg.falldetectionlib.fsm.WalkDetectionFSM.WalkEvent.EVENT_NO_EVENT;
import static com.example.tsigg.falldetectionlib.fsm.WalkDetectionFSM.WalkEvent.EVENT_SEGMENT_DUR_FAIL;
import static com.example.tsigg.falldetectionlib.fsm.WalkDetectionFSM.WalkEvent.EVENT_SEGMENT_DUR_OK;
import static com.example.tsigg.falldetectionlib.fsm.WalkDetectionFSM.WalkEvent.EVENT_STEP_LENGTH_FAIL;
import static com.example.tsigg.falldetectionlib.fsm.WalkDetectionFSM.WalkEvent.EVENT_STEP_LENGTH_OK;
import static com.example.tsigg.falldetectionlib.fsm.WalkDetectionFSM.WalkEvent.EVENT_STEP_REGULARITY_FAIL;
import static com.example.tsigg.falldetectionlib.fsm.WalkDetectionFSM.WalkEvent.EVENT_STEP_REGULARITY_OK;
import static com.example.tsigg.falldetectionlib.fsm.WalkDetectionFSM.WalkState.STATE_GROUP_END;
import static com.example.tsigg.falldetectionlib.fsm.WalkDetectionFSM.WalkState.STATE_GROUP_START;
import static com.example.tsigg.falldetectionlib.fsm.WalkDetectionFSM.WalkState.STATE_NO_STATE;
import static com.example.tsigg.falldetectionlib.fsm.WalkDetectionFSM.WalkState.STATE_SEGMENT_DUR_TEST;
import static com.example.tsigg.falldetectionlib.fsm.WalkDetectionFSM.WalkState.STATE_STEP_LENGTH_TEST;
import static com.example.tsigg.falldetectionlib.fsm.WalkDetectionFSM.WalkState.STATE_STEP_REGULARITY_TEST;
import static com.example.tsigg.falldetectionlib.fsm.WalkDetectionFSM.WalkState.STATE_WALK;


/**
 * Created by tsigg on 14/10/2016.
 */
public class WalkDetectionFSM {

    public enum WalkState {
        STATE_NO_STATE(-1),
        STATE_GROUP_START(0),
        STATE_GROUP_END(1),
        STATE_STEP_LENGTH_TEST(2),
        STATE_SEGMENT_DUR_TEST(3),
        STATE_STEP_REGULARITY_TEST(4),
        STATE_WALK(5);

        private int value;
        private WalkState(int value) {
            this.value = value;
        }
    }

    public enum WalkEvent {
        EVENT_NO_EVENT(0),
        EVENT_GROUP_PEAKS_FOUND(1),
        EVENT_GROUP_END_FOUND(2),
        EVENT_STEP_LENGTH_OK(3),
        EVENT_FIRST_GROUP(4),
        EVENT_STEP_LENGTH_FAIL(5),
        EVENT_SEGMENT_DUR_OK(6),
        EVENT_SEGMENT_DUR_FAIL(7),
        EVENT_STEP_REGULARITY_OK(8),
        EVENT_STEP_REGULARITY_FAIL(9);

        private int value;
        private WalkEvent(int value) {
            this.value = value;
        }
    }

    private static final WalkState[][] MATRIX_WALK = new WalkState[][]{
        {STATE_GROUP_START, STATE_GROUP_END, STATE_NO_STATE, STATE_NO_STATE, STATE_NO_STATE, STATE_NO_STATE, STATE_NO_STATE, STATE_NO_STATE, STATE_NO_STATE, STATE_NO_STATE},
        {STATE_GROUP_END, STATE_NO_STATE, STATE_STEP_LENGTH_TEST, STATE_NO_STATE, STATE_NO_STATE, STATE_NO_STATE, STATE_NO_STATE, STATE_NO_STATE, STATE_NO_STATE, STATE_NO_STATE},
        {STATE_STEP_LENGTH_TEST, STATE_NO_STATE, STATE_NO_STATE, STATE_SEGMENT_DUR_TEST, STATE_GROUP_START, STATE_GROUP_START, STATE_NO_STATE, STATE_NO_STATE, STATE_NO_STATE, STATE_NO_STATE},
        {STATE_SEGMENT_DUR_TEST, STATE_NO_STATE, STATE_NO_STATE, STATE_NO_STATE, STATE_NO_STATE, STATE_NO_STATE, STATE_STEP_REGULARITY_TEST, STATE_GROUP_START, STATE_NO_STATE, STATE_NO_STATE},
        {STATE_STEP_REGULARITY_TEST, STATE_NO_STATE, STATE_NO_STATE, STATE_NO_STATE, STATE_NO_STATE, STATE_NO_STATE, STATE_NO_STATE, STATE_NO_STATE, STATE_WALK, STATE_GROUP_START},
        {STATE_GROUP_START, STATE_NO_STATE, STATE_NO_STATE, STATE_NO_STATE, STATE_NO_STATE, STATE_NO_STATE, STATE_NO_STATE, STATE_NO_STATE, STATE_NO_STATE, STATE_NO_STATE}
    };

    private WalkState mCurrentStateWalk = STATE_GROUP_START, mNextStateWalk = STATE_GROUP_START;
    private WalkEvent mEventOccurredWalk = EVENT_NO_EVENT;

    private static double C_PEAK_INTENSITY = 1.1,
            C_GROUP_INT_MAX = 350,
            C_GROUP_DUR_MAX = 450,
            C_STEP_DUR_MIN = 300,
            C_STEP_DUR_MAX = 1000,
            C_STEP_DEV_MAX = 40,
            C_SEG_DUR_MIN = 6000;
    private double mGroupStartTime = 0, mGroupEndTime = 0, mLastPeakTime = 0;
    private boolean mFirstGroup = true, mWalkDetected = false;

    private InputFSM mPreviousInput, mCurrentInput, mNextInput;
    private double[] mGravityVector = new double[]{0.01, 0.99, 0.01};
    private ArrayList<double[]> mWalkSegment = new ArrayList<double[]>(10);
    private InputBufferFSM mBuffer = new InputBufferFSM(600);

    private static final String TAG = WalkDetectionFSM.class.getSimpleName();

    private static WalkDetectionFSM ourInstance = new WalkDetectionFSM();

    public static WalkDetectionFSM getInstance() {
        return ourInstance;
    }

    private WalkDetectionFSM() {
    }

    public WalkState getCurrentStateWalk() {
        return mCurrentStateWalk;
    }

    public WalkEvent getEventOccurredWalk() {
        return mEventOccurredWalk;
    }

    public WalkState getNextStateWalk() {
        return mNextStateWalk;
    }

    public boolean isWalkDetected() {
        return mWalkDetected;
    }

    public double[] getGravityVector() {
        return mGravityVector;
    }

    public void run(InputFSM newInput) {
        mPreviousInput = mCurrentInput;
        mCurrentInput = mNextInput;
        mNextInput = newInput;
        if (mPreviousInput == null)
            return;

        mWalkDetected = false;
        mBuffer.add(mCurrentInput);

        mCurrentStateWalk = mNextStateWalk;

        switch(mCurrentStateWalk) {

            case STATE_GROUP_START:
                if (mCurrentInput.getM() >= C_PEAK_INTENSITY && mCurrentInput.getM() >= mPreviousInput.getM() && mCurrentInput.getM() > mNextInput.getM()) {
                    mEventOccurredWalk = EVENT_GROUP_PEAKS_FOUND;
                    mGroupStartTime = mCurrentInput.getTime();
                    mLastPeakTime = mCurrentInput.getTime();
                } else
                    mEventOccurredWalk = EVENT_NO_EVENT;
                break;

            case STATE_GROUP_END:
                if (mCurrentInput.getTime() - mGroupStartTime > C_GROUP_DUR_MAX || mCurrentInput.getTime() - mLastPeakTime > C_GROUP_INT_MAX) {
                    mEventOccurredWalk = EVENT_GROUP_END_FOUND;
                    mGroupEndTime = mCurrentInput.getTime();
                    double myarray [] = {mGroupStartTime, mGroupEndTime, (mGroupStartTime + mGroupEndTime)/2.0 };
                    mWalkSegment.add(myarray);
                } else {
                    if (mCurrentInput.getM() >= C_PEAK_INTENSITY && mCurrentInput.getM() >= mPreviousInput.getM() && mCurrentInput.getM() > mNextInput.getM())
                        mLastPeakTime = mCurrentInput.getTime();

                    mEventOccurredWalk = EVENT_NO_EVENT;
                }
                break;

            case STATE_STEP_LENGTH_TEST:
                if (mFirstGroup) {
                    mEventOccurredWalk = EVENT_FIRST_GROUP;
                    mFirstGroup = false;
                } else {
                    double test = stepLengthTest();
                    if (test > C_STEP_DUR_MIN && test < C_STEP_DUR_MAX)
                        mEventOccurredWalk = EVENT_STEP_LENGTH_OK;
                    else {
                        mEventOccurredWalk = EVENT_STEP_LENGTH_FAIL;
                        mWalkSegment.clear();
                        mFirstGroup = true;
                    }
                }
                break;

            case STATE_SEGMENT_DUR_TEST:
                double test = segmentDurTest();
                if (test < C_SEG_DUR_MIN)
                    mEventOccurredWalk = EVENT_SEGMENT_DUR_FAIL;
                else
                    mEventOccurredWalk = EVENT_SEGMENT_DUR_OK;
                break;

            case STATE_STEP_REGULARITY_TEST:
                ArrayList<Double> osd = new ArrayList<Double>(mWalkSegment.size()/2);
                ArrayList<Double> esd = new ArrayList<Double>(mWalkSegment.size()/2);
                for (int k = 0; k< mWalkSegment.size(); k++) {
                    double diff = mWalkSegment.get(k)[1] - mWalkSegment.get(k)[0];
                    if (k % 2 == 1)    // ODD
                        osd.add(diff);
                    else
                        esd.add(diff);
                }
                double stdO = Math.sqrt(StatUtils.variance(toArray(osd)));
                double stdE = Math.sqrt(StatUtils.variance(toArray(esd)));
                if ( stdO <= C_STEP_DEV_MAX && stdE <= C_STEP_DEV_MAX)
                    mEventOccurredWalk = EVENT_STEP_REGULARITY_OK;
                else {
                    mEventOccurredWalk = EVENT_STEP_REGULARITY_FAIL;
                    mWalkSegment.remove(0);
                }
                break;

            case STATE_WALK:
                mEventOccurredWalk = EVENT_NO_EVENT;
                double idStart = mWalkSegment.get(0)[0];
                double idEnd = mWalkSegment.get(mWalkSegment.size()-1)[1];

                double[] aX = mBuffer.getXAxisRange(idStart, idEnd),
                        aY = mBuffer.getYAxisRange(idStart, idEnd),
                        aZ = mBuffer.getZAxisRange(idStart, idEnd);
                double[] g = new double[3];

                g[0] = StatUtils.mean(aX);
                g[1] = StatUtils.mean(aY);
                g[2] = StatUtils.mean(aZ);

                if (Math.abs( Math.sqrt(g[0]*g[0]+g[1]*g[1]+g[2]*g[2]) - 1 ) < 0.05)
                    mGravityVector = g.clone();

                mWalkSegment.clear();

                mFirstGroup = true;
                mWalkDetected = true;
                mGroupStartTime = 0;
                mGroupEndTime = 0;
                mLastPeakTime = 0;
                break;

            default:
                break;
        }

        mNextStateWalk = MATRIX_WALK[mCurrentStateWalk.value][mEventOccurredWalk.value];

    }

    private double[] toArray(ArrayList<Double> a) {
        double[] res = new double[a.size()];
        for (int i=0; i<a.size(); i++)
            res[i] = a.get(i);
        return res;
    }

    private double segmentDurTest() {
        double res = 0;
        int size = mWalkSegment.size();
        res = mWalkSegment.get(size-1)[1] - mWalkSegment.get(0)[0];

        return res;
    }

    private double stepLengthTest() {
        double res = 0;
        int size = mWalkSegment.size();
        res = mWalkSegment.get(size-1)[2] - mWalkSegment.get(size-2)[2];

        return res;
    }
}

/*

@startuml

skinparam state {
    FontSize 18
    FontName Segoe UI
    AttributeFontSize 16
    AttributeFontName Segoe UI
    arrowFontSize 16
}

[*] --> Group_Start  : reset segment
Group_Start --> Group_End : group peaks\nfound
Group_End --> Step_Length_Test : group end\nfound
Step_Length_Test --> Group_Start : step length\nfailed
Step_Length_Test --> Group_Start : first group
Step_Length_Test --> Segment_Duration_Test : step length\nOK
Segment_Duration_Test --> Group_Start : segment duration\nfailed
Segment_Duration_Test --> Step_Regularity_Test : segment duration\nOK
Step_Regularity_Test --> Group_Start : walk segment\nfailed
Step_Regularity_Test --> Walk : walk segment\ndetected
Walk --> Group_Start

Walk : estimate gravity

state "Group Start" as Group_Start
state "Group End" as Group_End
state "Step Length Test" as Step_Length_Test
state "Segment Duration Test" as Segment_Duration_Test
state "Step Regularity Test" as Step_Regularity_Test

@enduml

*/
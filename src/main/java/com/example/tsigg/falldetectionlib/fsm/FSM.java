package com.example.tsigg.falldetectionlib.fsm;

import android.content.Context;
import android.content.Intent;

import com.example.tsigg.falldetectionlib.LibUtility;

import java.util.logging.Logger;

/**
 * Created by tsigg on 15/10/2016.
 */
public class FSM {

    public static final String TAG = FSM.class.getSimpleName();
    private WalkDetectionFSM mWalkDetectionFSM;
    private FallDetectionFSM mFallDetectionFSM;
    private boolean mFallDetected, mWalkDetected;
    private Context mContext;
    private Logger mLogger;

    public FSM(Context context) {
        mWalkDetectionFSM = WalkDetectionFSM.getInstance();
        mFallDetectionFSM = FallDetectionFSM.getInstance();

        mContext = context.getApplicationContext();
        mLogger = LibUtility.getFallDetectionLogger(context);
        //Log.d(TAG, "FSM created");
    }

    public void run(InputFSM inputFSM) {
        //Log.d(TAG, inputFSM.getTime() + "," + inputFSM.getX() + "," + inputFSM.getY() + "," + inputFSM.getZ());
        mWalkDetectionFSM.run(inputFSM);
        mFallDetectionFSM.run(inputFSM);

        if (mWalkDetectionFSM.isWalkDetected()) {
            mFallDetectionFSM.setGravityVector(
                    mWalkDetectionFSM.getGravityVector()
            );
            mWalkDetected = true;
        } else
            mWalkDetected = false;

        if (mFallDetectionFSM.isFallDetected()) {
            mFallDetected = true;
            InputBufferFSM input = mFallDetectionFSM.getClassificationInput();
            double impactStartTime = mFallDetectionFSM.getImpactStartTime();
            double impactEndTime = mFallDetectionFSM.getImpactEndTime();
            double peakTime = mFallDetectionFSM.getPeakTime();
            double[] gravityVector = mWalkDetectionFSM.getGravityVector();

            Intent intent = new Intent();
            intent.setAction(LibUtility.ACTION_FSM_FALL);
            intent.putExtra(LibUtility.TAG_CLASSIFY_INPUT, input);
            intent.putExtra(LibUtility.TAG_CLASSIFY_IMPACT_START, impactStartTime);
            intent.putExtra(LibUtility.TAG_CLASSIFY_IMPACT_END, impactEndTime);
            intent.putExtra(LibUtility.TAG_CLASSIFY_PEAK, peakTime);
            intent.putExtra(LibUtility.TAG_CLASSIFY_GRAVITY, gravityVector);
            mContext.sendBroadcast(intent);
            //Log.d(TAG, Utility.ACTION_FSM_FALL + " broadcast sent");
            mLogger.fine(LibUtility.ACTION_FSM_FALL + " broadcast sent");
            mLogger.fine("IMPACT_START: " + impactStartTime);
            mLogger.fine("IMPACT_END: " + impactEndTime);
            mLogger.fine("PEAK_TIME: " + peakTime);

        } else
            mFallDetected = false;
    }

    public String getFSMFallTransition() {
        FallDetectionFSM.FallState cfs = mFallDetectionFSM.getCurrentStateFall();
        FallDetectionFSM.FallState nfs = mFallDetectionFSM.getNextStateFall();
        FallDetectionFSM.FallEvent ofe = mFallDetectionFSM.getEventOccurredFall();
        String res = cfs + " + " + ofe + " -> " + nfs;
        return res;
    }

    public String getFSMWalkTransition() {
        WalkDetectionFSM.WalkState cws = mWalkDetectionFSM.getCurrentStateWalk();
        WalkDetectionFSM.WalkState nws = mWalkDetectionFSM.getNextStateWalk();
        WalkDetectionFSM.WalkEvent owe = mWalkDetectionFSM.getEventOccurredWalk();
        String res = cws + " + " + owe + " -> " + nws;
        return res;
    }

}

package com.example.tsigg.falldetectionlib.fsm;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by tsigg on 14/10/2016.
 */

public class InputBufferFSM implements Serializable {
    private ArrayList<Double> mTime;
    private ArrayList<Double> mXAxis;
    private ArrayList<Double> mYAxis;
    private ArrayList<Double> mZAxis;
    private int mMaxSize;

    public InputBufferFSM(int size) {
        mTime = new ArrayList<Double>(size);
        mXAxis = new ArrayList<Double>(size);
        mYAxis = new ArrayList<Double>(size);
        mZAxis = new ArrayList<Double>(size);
        mMaxSize = size;
    }

    public InputBufferFSM(double[] time, double[] x, double[] y, double[] z, int size) {
        mMaxSize = (size > time.length) ? size : time.length;
        mTime = new ArrayList<Double>(mMaxSize);
        mXAxis = new ArrayList<Double>(mMaxSize);
        mYAxis = new ArrayList<Double>(mMaxSize);
        mZAxis = new ArrayList<Double>(mMaxSize);

        for (int i=0; i<time.length; i++) {
            mTime.add(time[i]);
            mXAxis.add(x[i]);
            mYAxis.add(y[i]);
            mZAxis.add(z[i]);
        }
    }


    public int add(InputFSM inputFSM) {

        if (mTime.size() > mMaxSize) {
            mTime.remove(0);
            mXAxis.remove(0);
            mYAxis.remove(0);
            mZAxis.remove(0);
        }

        mTime.add(inputFSM.getTime());
        mXAxis.add(inputFSM.getX());
        mYAxis.add(inputFSM.getY());
        mZAxis.add(inputFSM.getZ());

        return mTime.size();
    }

    public int getSize() {
        return mMaxSize;
    }

    public InputFSM get(int index) {
        double t = mTime.get(index);
        double x = mXAxis.get(index);
        double y = mYAxis.get(index);
        double z = mZAxis.get(index);
        return new InputFSM(t, x, y, z);
    }

    public double[] getTime() {
        double[] t = new double[mTime.size()];
        for (int i=0; i<t.length; i++)
            t[i] = mTime.get(i);
        return t;
    }

    public double[] getXAxis() {
        Double[] temp = new Double[mXAxis.size()];
        double[] x = new double[mXAxis.size()];

        temp= mXAxis.toArray(temp);
        for (int i=0; i<temp.length; i++) {
            x[i] = temp[i];
        }
        return x;
    }

    public double[] getYAxis() {
        Double[] temp = new Double[mYAxis.size()];
        double[] y = new double[mYAxis.size()];

        temp= mYAxis.toArray(temp);
        for (int i=0; i<temp.length; i++) {
            y[i] = temp[i];
        }
        return y;
    }

    public double[] getZAxis() {
        Double[] temp = new Double[mZAxis.size()];
        double[] z = new double[mZAxis.size()];

        temp= mZAxis.toArray(temp);
        for (int i=0; i<temp.length; i++) {
            z[i] = temp[i];
        }
        return z;
    }

    public double[] getMagnitude() {
        double[] res = new double[mTime.size()];

        for (int i=0; i<res.length; i++) {
            double x = mXAxis.get(i);
            double y = mYAxis.get(i);
            double z = mZAxis.get(i);
            res[i] = Math.sqrt(x*x + y*y + z*z);
        }

        return res;
    }

    public double[] getTimeRange(int start, int end) {
        double[] t = new double[end-start+1];
        Double[] temp = new Double[end-start+1];
        temp= mTime.subList(start, end+1).toArray(temp);
        for (int i=0; i<temp.length; i++) {
            t[i] = temp[i];
        }
        return t;
    }

    public double[] getTimeRange(double start, double end) {
        int indexStart = indexOfTime(start);
        int indexEnd = indexOfTime(end);
        return getTimeRange(indexStart, indexEnd);
    }

    public double[] getTimeRange(int p, boolean greater) {
        if (greater) {
            int index2 = mTime.size() - 1;
            return getTimeRange(p, index2);
        } else {
            int index2 = 0;
            return getTimeRange(index2,p);
        }
    }

    public double[] getTimeRange(double p, boolean greater) {
        int index1 = indexOfTime(p);
        if (greater) {
            int index2 = mTime.size() - 1;
            return getTimeRange(index1, index2);
        } else {
            int index2 = 0;
            return getTimeRange(index2,index1);
        }
    }

    public double[] getXAxisRange(int start, int end) {
        Double[] temp = new Double[end-start+1];
        double[] x = new double[end-start+1];
        temp = mXAxis.subList(start, end+1).toArray(temp);

        for (int i=0; i<temp.length; i++)
            x[i] = temp[i];
        return x;
    }

    public double[] getXAxisRange(double start, double end) {
        int indexStart = indexOfTime(start);
        int indexEnd = indexOfTime(end);
        return getXAxisRange(indexStart, indexEnd);
    }

    public double[] getXAxisRange(int p, boolean greater) {
        if (greater) {
            int index2 = mXAxis.size() - 1;
            return getXAxisRange(p, index2);
        } else {
            int index2 = 0;
            return getXAxisRange(index2,p);
        }
    }

    public double[] getXAxisRange(double p, boolean greater) {
        int index1 = indexOfTime(p);
        if (greater) {
            int index2 = mXAxis.size() - 1;
            return getXAxisRange(index1, index2);
        } else {
            int index2 = 0;
            return getXAxisRange(index2,index1);
        }
    }

    public double[] getYAxisRange(int start, int end) {
        Double[] temp = new Double[end-start+1];
        double[] y = new double[end-start+1];
        temp = mYAxis.subList(start, end+1).toArray(temp);

        for (int i=0; i<temp.length; i++)
            y[i] = temp[i];
        return y;
    }

    public double[] getYAxisRange(double start, double end) {
        int indexStart = indexOfTime(start);
        int indexEnd = indexOfTime(end);
        return getYAxisRange(indexStart, indexEnd);
    }

    public double[] getYAxisRange(int p, boolean greater) {
        if (greater) {
            int index2 = mYAxis.size() - 1;
            return getYAxisRange(p, index2);
        } else {
            int index2 = 0;
            return getYAxisRange(index2,p);
        }
    }

    public double[] getYAxisRange(double p, boolean greater) {
        int index1 = indexOfTime(p);
        if (greater) {
            int index2 = mYAxis.size() - 1;
            return getYAxisRange(index1, index2);
        } else {
            int index2 = 0;
            return getYAxisRange(index2,index1);
        }
    }

    public double[] getZAxisRange(int start, int end) {
        Double[] temp = new Double[end-start+1];
        double[] z = new double[end-start+1];
        temp = mZAxis.subList(start, end+1).toArray(temp);

        for (int i=0; i<temp.length; i++)
            z[i] = temp[i];
        return z;
    }

    public double[] getZAxisRange(double start, double end) {
        int indexStart = indexOfTime(start);
        int indexEnd = indexOfTime(end);
        return getZAxisRange(indexStart, indexEnd);
    }

    public double[] getZAxisRange(int p, boolean greater) {
        if (greater) {
            int index2 = mZAxis.size() - 1;
            return getZAxisRange(p, index2);
        } else {
            int index2 = 0;
            return getZAxisRange(index2,p);
        }
    }

    public double[] getZAxisRange(double p, boolean greater) {
        int index1 = indexOfTime(p);
        if (greater) {
            int index2 = mZAxis.size() - 1;
            return getZAxisRange(index1, index2);
        } else {
            int index2 = 0;
            return getZAxisRange(index2,index1);
        }
    }

    public double[] getMagnitudeRange(int start, int end) {
        double[] x = getXAxisRange(start, end);
        double[] y = getYAxisRange(start, end);
        double[] z = getZAxisRange(start, end);
        double[] res = new double[x.length];

        for (int i=0; i<res.length; i++)
            res[i] = Math.sqrt(x[i]*x[i] + y[i]*y[i] + z[i]*z[i]);

        return res;
    }

    public double[] getMagnitudeRange(double start, double end) {
        int indexStart = indexOfTime(start);
        int indexEnd = indexOfTime(end);
        return getMagnitudeRange(indexStart, indexEnd);
    }

    public double[] getMagnitudeRange(int p, boolean greater) {
        if (greater) {
            int index2 = mTime.size() - 1;
            return getMagnitudeRange(p, index2);
        } else {
            int index2 = 0;
            return getMagnitudeRange(index2,p);
        }
    }

    public double[] getMagnitudeRange(double p, boolean greater) {
        int index1 = indexOfTime(p);
        if (greater) {
            int index2 = mTime.size() - 1;
            return getMagnitudeRange(index1, index2);
        } else {
            int index2 = 0;
            return getMagnitudeRange(index2,index1);
        }
    }

    public InputFSM getValuesAtTime(int index) {
        return new InputFSM(mTime.get(index), mXAxis.get(index), mYAxis.get(index), mZAxis.get(index));
    }

    public InputFSM getValuesAtTime(double t) {
        int index = indexOfTime(t);
        return getValuesAtTime(index);
    }

    public InputFSM[] getValuesAtTimeRange(int start, int end) {
        double[] x = getXAxisRange(start, end);
        double[] y = getYAxisRange(start, end);
        double[] z = getZAxisRange(start, end);
        double[] t = getTimeRange(start, end);
        InputFSM[] res = new InputFSM[x.length];

        for (int i=0; i<res.length; i++) {
            //res[i].setX(x[i]);
            //res[i].setY(y[i]);
            //res[i].setZ(z[i]);
            //res[i].setTime(t[i]);
            res[i] = new InputFSM(t[i], x[i], y[i], z[i]);
        }

        return res;
    }

    public InputBufferFSM getValuesAtTimeRange(double start, double end) {
        double[] t = getTimeRange(start, end);
        double[] x = getXAxisRange(start, end);
        double[] y = getYAxisRange(start, end);
        double[] z = getZAxisRange(start, end);

        return new InputBufferFSM(t,x,y,z,t.length);
    }
/*
    public InputFSM[] getValuesAtTimeRange(double start, double end) {
        int indexStart = indexOfTime(start);
        int indexEnd = indexOfTime(end);
        return getValuesAtTimeRange(indexStart, indexEnd);
    }
*/
    public int indexOfTime(double t) {
        int index = -1;
        if (t < mTime.get(0))
            return 0;
        for (int i = 0; i< mTime.size()-1; i++) {
            //if (Precision.equals(mTime.get(i), t, 10.0f)) {
            if (mTime.get(i) <= t && mTime.get(i+1) > t) {
                index = i;
                break;
            }
        }
        if (index == -1)
            index = mTime.size()-1;
        return index;
    }

    public int indexOfTime(double t, boolean revDir) {
        int index = 0;
        if (revDir) {
            for (int i = mTime.size()-1; i >= 0 ; i--) {
                //if (Precision.equals(mTime.get(i), t, 10.0f)) {
                if (mTime.get(i) - t >= 0) {
                    index = i;
                    break;
                }
            }
        } else
            index = indexOfTime(t);
        return index;
    }
}

package com.example.tsigg.falldetectionlib.fsm;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.stat.StatUtils;

import static com.example.tsigg.falldetectionlib.fsm.FallDetectionFSM.FallEvent.EVENT_ACTIVITY_FOUND;
import static com.example.tsigg.falldetectionlib.fsm.FallDetectionFSM.FallEvent.EVENT_ACTIVITY_NOT_FOUND;
import static com.example.tsigg.falldetectionlib.fsm.FallDetectionFSM.FallEvent.EVENT_BOUNCING_END;
import static com.example.tsigg.falldetectionlib.fsm.FallDetectionFSM.FallEvent.EVENT_EXCEED_PEAK;
import static com.example.tsigg.falldetectionlib.fsm.FallDetectionFSM.FallEvent.EVENT_NO_EVENT;
import static com.example.tsigg.falldetectionlib.fsm.FallDetectionFSM.FallEvent.EVENT_POST_FALL_END;
import static com.example.tsigg.falldetectionlib.fsm.FallDetectionFSM.FallState.STATE_ACTIVITY;
import static com.example.tsigg.falldetectionlib.fsm.FallDetectionFSM.FallState.STATE_FALL;
import static com.example.tsigg.falldetectionlib.fsm.FallDetectionFSM.FallState.STATE_NO_STATE;
import static com.example.tsigg.falldetectionlib.fsm.FallDetectionFSM.FallState.STATE_POST_FALL;
import static com.example.tsigg.falldetectionlib.fsm.FallDetectionFSM.FallState.STATE_POST_PEAK;
import static com.example.tsigg.falldetectionlib.fsm.FallDetectionFSM.FallState.STATE_SAMPL;


/**
 * Created by tsigg on 14/10/2016.
 */
public class FallDetectionFSM {

    public enum FallState {
        STATE_NO_STATE(-1),
        STATE_SAMPL(0),
        STATE_POST_PEAK(1),
        STATE_POST_FALL(2),
        STATE_ACTIVITY(3),
        STATE_FALL(4);

        private int value;
        private FallState(int value) {
            this.value = value;
        }
    }

    public enum FallEvent {
        EVENT_NO_EVENT(0),
        EVENT_EXCEED_PEAK(1),
        EVENT_BOUNCING_END(2),
        EVENT_POST_FALL_END(3),
        EVENT_ACTIVITY_NOT_FOUND(4),
        EVENT_ACTIVITY_FOUND(5);

        private int value;
        private FallEvent(int value) {
            this.value = value;
        }
    }

    private static final FallState[][] MATRIX_WALK = new FallState[][]{
            {STATE_SAMPL, STATE_POST_PEAK, STATE_NO_STATE, STATE_NO_STATE, STATE_NO_STATE, STATE_NO_STATE},
            {STATE_POST_PEAK, STATE_POST_PEAK, STATE_POST_FALL, STATE_NO_STATE, STATE_NO_STATE, STATE_NO_STATE},
            {STATE_POST_FALL, STATE_POST_PEAK, STATE_NO_STATE, STATE_ACTIVITY, STATE_NO_STATE, STATE_NO_STATE},
            {STATE_ACTIVITY, STATE_NO_STATE, STATE_NO_STATE, STATE_NO_STATE, STATE_FALL, STATE_SAMPL},
            {STATE_SAMPL, STATE_NO_STATE, STATE_NO_STATE, STATE_NO_STATE, STATE_NO_STATE, STATE_NO_STATE}
    };

    private FallState mCurrentStateFall = STATE_SAMPL, mNextStateFall = STATE_SAMPL;
    private FallEvent mEventOccurredFall = EVENT_NO_EVENT;

    private static double C_PEAK = 1.8,
            C_BOUNCING_TIMER = 1000,
            C_POST_FALL_TIMER = 1500,
            C_IMPACT_START_LOW = -1200,
            C_IMPACT_START_UTH = 1.4,
            C_IMPACT_START_LTH = 0.8,
            C_IMPACT_END_TH = 1.4,
            C_IMPACT_END_HIGH = 1000,
            C_ACTIVITY_TEST_LOW = 1500,
            C_ACTIVITY_TEST_HIGH = 2500,
            C_ACTIVITY_TEST_INTERVAL = C_ACTIVITY_TEST_HIGH - C_ACTIVITY_TEST_LOW,
            C_ACTIVITY_TEST_TH = 0.15,
            C_ANGLE_TEST_TH = 0.5,
            C_CLASSIFICATION_LOW = -2500,
            C_CLASSIFICATION_HIGH = 1000;
    private double mImpactStartTime = 0, mImpactEndTime = 0, mPeakValue = 0, mPeakTime = 0, mLastPeakValue = 0, mLastPeakTime = 0;
    private int mImpactEndIndex = 0, mImpactStartIndex = 0, mPeakIndex = 0;
    private boolean mFallDetected = false;

    private InputFSM mPreviousInput, mCurrentInput;
    private InputBufferFSM mClassificationInput;
    private double[] mGravityVector = null;
    private InputBufferFSM mBuffer = new InputBufferFSM(600);

    private static final String TAG = FallDetectionFSM.class.getSimpleName();

    private static FallDetectionFSM ourInstance = new FallDetectionFSM();

    public static FallDetectionFSM getInstance() {
        return ourInstance;
    }

    private FallDetectionFSM() {

    }

    public void setGravityVector(double[] gravityVector) {
        this.mGravityVector = gravityVector;
    }

    public FallState getCurrentStateFall() {
        return mCurrentStateFall;
    }

    public FallState getNextStateFall() {
        return mNextStateFall;
    }

    public FallEvent getEventOccurredFall() {
        return mEventOccurredFall;
    }

    public boolean isFallDetected() {
        return mFallDetected;
    }

    public InputBufferFSM getClassificationInput() {
        return mClassificationInput;
    }

    public double getImpactEndTime() {
        return mImpactEndTime;
    }

    public double getImpactStartTime() {
        return mImpactStartTime;
    }

    public double getPeakTime() {
        return mPeakTime;
    }

    public void run(InputFSM newInput) {
        mPreviousInput = mCurrentInput;
        mCurrentInput = newInput;
        if (mPreviousInput == null)
            return;

        mFallDetected = false;
        mBuffer.add(mCurrentInput);

        mCurrentStateFall = mNextStateFall;

        switch(mCurrentStateFall) {
            case STATE_SAMPL:
                if (mCurrentInput.getM() >= C_PEAK && mCurrentInput.getM() > mPreviousInput.getM()) {
                    mEventOccurredFall = EVENT_EXCEED_PEAK;
                    mLastPeakTime = mCurrentInput.getTime();
                    mLastPeakValue = mCurrentInput.getM();
                    if (mLastPeakValue >= mPeakValue) {
                        mPeakValue = mLastPeakValue;
                        mPeakTime = mLastPeakTime;
                    }
                } else
                    mEventOccurredFall = EVENT_NO_EVENT;
                break;

            case STATE_POST_PEAK:
                if (mCurrentInput.getTime() - mLastPeakTime < C_BOUNCING_TIMER) {
                    if (mCurrentInput.getM() >= C_PEAK && mCurrentInput.getM() > mPreviousInput.getM()) {
                        mEventOccurredFall = EVENT_EXCEED_PEAK;
                        mLastPeakTime = mCurrentInput.getTime();
                        mLastPeakValue = mCurrentInput.getM();
                        if (mLastPeakValue >= mPeakValue) {
                            mPeakValue = mLastPeakValue;
                            mPeakTime = mLastPeakTime;
                        }
                    } else
                        mEventOccurredFall = EVENT_NO_EVENT;
                } else
                    mEventOccurredFall = EVENT_BOUNCING_END;
                break;

            case STATE_POST_FALL:
                if (mCurrentInput.getTime() - mLastPeakTime < C_POST_FALL_TIMER) {
                    if (mCurrentInput.getM() >= C_PEAK && mCurrentInput.getM() > mPreviousInput.getM()) {
                        mEventOccurredFall = EVENT_EXCEED_PEAK;
                        mLastPeakTime = mCurrentInput.getTime();
                        mLastPeakValue = mCurrentInput.getM();
                        if (mLastPeakValue >= mPeakValue) {
                            mPeakValue = mLastPeakValue;
                            mPeakTime = mLastPeakTime;
                        }
                    } else
                        mEventOccurredFall = EVENT_NO_EVENT;
                } else {
                    mEventOccurredFall = EVENT_POST_FALL_END;
                }
                break;

            case STATE_ACTIVITY:
                if (mCurrentInput.getTime() - mLastPeakTime - C_ACTIVITY_TEST_LOW >= C_ACTIVITY_TEST_INTERVAL) {

                    double[] activityTestInput = mBuffer.getMagnitudeRange(
                            mLastPeakTime + C_ACTIVITY_TEST_LOW,
                            mLastPeakTime + C_ACTIVITY_TEST_HIGH
                    );
                    double std = Math.sqrt(StatUtils.variance(activityTestInput));
                    double angle = 0;
                    if (mGravityVector != null) {
                        Vector3D grav = new Vector3D(mGravityVector);
                        Vector3D acc = new Vector3D(
                                StatUtils.percentile(
                                        mBuffer.getXAxisRange(mLastPeakTime + C_ACTIVITY_TEST_LOW,
                                                mLastPeakTime + C_ACTIVITY_TEST_HIGH), 50),
                                StatUtils.percentile(
                                        mBuffer.getYAxisRange(mLastPeakTime + C_ACTIVITY_TEST_LOW,
                                                mLastPeakTime + C_ACTIVITY_TEST_HIGH), 50),
                                StatUtils.percentile(
                                        mBuffer.getZAxisRange(mLastPeakTime + C_ACTIVITY_TEST_LOW,
                                                mLastPeakTime + C_ACTIVITY_TEST_HIGH), 50)
                        );
                        angle = Vector3D.angle(grav, acc);
                    }
                    else
                        angle = 1;
                    if (std <= C_ACTIVITY_TEST_TH && angle >= C_ANGLE_TEST_TH) {
                        mEventOccurredFall = EVENT_ACTIVITY_NOT_FOUND;
                        if (mBuffer.indexOfTime(mPeakTime) == 0) {
                            double[] temp = mBuffer.getMagnitudeRange(mPeakTime, mLastPeakTime);
                            for (int i=0; i<temp.length; i++)
                                if (temp[i] > C_PEAK) {
                                    mPeakTime = mBuffer.getTime()[i];
                                    break;
                                }
                        }
                    } else {
                        mPeakValue = 0;
                        mLastPeakValue = 0;
                        mEventOccurredFall = EVENT_ACTIVITY_FOUND;
                    }

                } else
                    mEventOccurredFall = EVENT_NO_EVENT;

                break;

            case STATE_FALL:
                mFallDetected = true;
                mEventOccurredFall = EVENT_NO_EVENT;
                // Find Peak, Impact End, Impact Start
                mPeakIndex = mBuffer.indexOfTime(mPeakTime);
                double[] temp = mBuffer.getMagnitudeRange(
                        mPeakTime,
                        mPeakTime + C_IMPACT_END_HIGH
                );
                mImpactEndIndex = findImpactEnd(temp);
                mImpactEndIndex += mPeakIndex;
                mImpactEndTime = mBuffer.get(mImpactEndIndex).getTime();
                int index = mBuffer.indexOfTime(mImpactEndTime + C_IMPACT_START_LOW);
                //temp = mBuffer.getMagnitudeRange(index, mImpactEndIndex);
                temp = mBuffer.getMagnitudeRange(index, mPeakIndex);
                mImpactStartIndex = findImpactStart(temp);
                if (mImpactStartIndex == 0)
                    mImpactStartIndex = mPeakIndex;
                else
                    mImpactStartIndex += index;
                mImpactStartTime = mBuffer.get(mImpactStartIndex).getTime();

                mClassificationInput = mBuffer.getValuesAtTimeRange(
                    mPeakTime + C_CLASSIFICATION_LOW,
                    mPeakTime + C_CLASSIFICATION_HIGH
                );

                mPeakValue = 0;
                break;

            default:
                break;
        }

        mNextStateFall = MATRIX_WALK[mCurrentStateFall.value][mEventOccurredFall.value];

    }

    private int findImpactStart(double[] a) {
        int index = 0;
        boolean flag = false;
        for (int i=0; i<a.length; i++) {
            if (a[i] < C_IMPACT_START_LTH) flag = true;
            else if (a[i] > C_IMPACT_START_UTH && flag) {
                index = i;
                flag = false;
                break;
            }
        }

        return index;
    }

    private int findImpactEnd(double[] a) {
        int index=0;
        for (int i=a.length-1; i>=0; i--) {
            if (a[i] > C_IMPACT_END_TH) {
                index = i;
                break;
            }
        }
        return index;
    }


}

/*

@startuml

skinparam state {
    FontSize 18
    FontName Segoe UI
    AttributeFontSize 16
    AttributeFontName Segoe UI
    arrowFontSize 15
}

[*] --> Sampling : start
Sampling --> Post_Peak : peak detected
Post_Peak --> Post_Peak : peak detected
Post_Peak --> Post_Fall : bouncing timer end
Post_Fall --> Post_Peak : peak detected
Post_Fall --> Activity_Test : post-fall timer end
Activity_Test -right-> Fall : activity not found
Activity_Test -up-> Sampling : activity found
Fall -up-> Sampling

Sampling : store sample
Post_Peak : (re)start bouncing timer
Post_Fall : (re)start post-fall timer
Activity_Test : std test\norientation angle test
Fall : extract segment of interest\nsend data for classification

state "Post Peak" as Post_Peak
state "Post Fall" as Post_Fall
state "Activity Test" as Activity_Test
@enduml

*/

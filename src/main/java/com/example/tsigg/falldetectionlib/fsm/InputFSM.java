package com.example.tsigg.falldetectionlib.fsm;

import java.io.Serializable;

/**
 * Created by tsigg on 14/10/2016.
 */

public class InputFSM implements Serializable {
    private double time;
    private double x;
    private double y;
    private double z;
    private double m;

    public void setTime(double time) {
        this.time = time;
    }
    public void setX(double x) {
        this.x = x;
    }
    public void setY(double y) {
        this.y = y;
    }
    public void setZ(double z) {
        this.z = z;
    }
    public void setM(double m) {
        this.m = m;
    }

    public double getM() {
        return m;
    }
    public double getTime() {
        return time;
    }
    public double getX() {
        return x;
    }
    public double getY() {
        return y;
    }
    public double getZ() {
        return z;
    }

    public String toString() {
        return "Time: " + time + ", X: " + x + ", Y: " + y + ", Z: " + z + ", M: " + m;
    }

    public InputFSM(double time, double x, double y, double z) {
        this.time = time;
        this.x = x;
        this.y = y;
        this.z = z;
        this.m = Math.sqrt(x*x + y*y + z*z);
    }

    public InputFSM() {
        this.time = System.currentTimeMillis();
        this.x = 1f;
        this.y = 1f;
        this.z = 1f;
        this.m = Math.sqrt(x*x + y*y + z*z);
    }



}

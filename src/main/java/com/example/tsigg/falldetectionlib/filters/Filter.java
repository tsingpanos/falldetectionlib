package com.example.tsigg.falldetectionlib.filters;

/**
 * Created by tsigg on 2/1/2017.
 */

public interface Filter {
    double[] addSamples(double[] acceleration);
    void setTimeConstant(double timeConstant);
}

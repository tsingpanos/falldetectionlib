package com.example.tsigg.falldetectionlib.filters;

/**
 * Created by tsigg on 9/10/2016.
 */

public class LowPassFilterGravity implements Filter{
    private static final String TAG = LowPassFilterGravity.class.getSimpleName();

    private double filterCoefficient = 0.5;

    // Gravity and linear accelerations components for the
    // Wikipedia low-pass filter

    private double[] gravity = new double[]
            { 0, 0, 0 };

    // Raw accelerometer data
    private double[] input = new double[]
            { 0, 0, 0 };

    /**
     * Add a sample.
     *
     * @param acceleration
     *            The acceleration data.
     * @return Returns the output of the filter.
     */
    @Override
    public double[] addSamples(double[] acceleration)
    {
        System.arraycopy(acceleration, 0, input, 0, acceleration.length);

        double oneMinusCoeff = (1.0 - filterCoefficient);

        gravity[0] = filterCoefficient * gravity[0] + oneMinusCoeff * input[0];
        gravity[1] = filterCoefficient * gravity[1] + oneMinusCoeff * input[1];
        gravity[2] = filterCoefficient * gravity[2] + oneMinusCoeff * input[2];

        return gravity;
    }

    /**
     * The complementary filter coefficient, a floating point value between 0-1,
     * exclusive of 0, inclusive of 1.
     *
     * @param filterCoefficient
     *              The filter coefficient in the interval (0,1]
     */
    @Override
    public void setTimeConstant(double filterCoefficient)
    {
        this.filterCoefficient = filterCoefficient;
    }
}

package com.example.tsigg.falldetectionlib.filters;

/**
 * Created by tsigg on 2/1/2017.
 */

public class NoFilter implements Filter {

    public NoFilter() {

    }

    public double[] addSamples(double[] acceleration) {
        return acceleration;
    }

    public void setTimeConstant(double timeConstant) {
        return;
    }
}

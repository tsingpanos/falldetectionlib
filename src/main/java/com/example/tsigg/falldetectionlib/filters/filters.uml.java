/*
@startuml
package filters {
    interface Filter
    Filter <|.le. class LowPass
    Filter <|.do. class LowPassGravity
    Filter <|.do. class Mean
    Filter <|.ri. class Median
}
interface Filter {
    +addSamples(Double[]) : Double[]
    +setTimeConstant(Double);
}
class LowPass {
    -mTimeConstant : Double
    -mAlpha : Double;
    -mDt : Double
    -mTimestamp : Double;
    -mStartTime : Double;
    -mCount : Integer
    -mOutput : Double[]
    -mInput : Double[]
}

class LowPassGravity {
    -mFilterCoefficient : Double
    -mGravity : Double[]
    -mInput : Double[]
}

class Mean {
    -mTimeConstant : Double
    -mStartTime : Double
    -mTimestamp : Double
    -mHz : Double
    -mCount : Integer
    -mFilterWindow : Integer
    -mDataInit : Boolean
    -mDataLists : ArrayList
    -getMean(data :List) : Double
}

class Median {
    -mTimeConstant : Double
    -mStartTime : Double
    -mTimestamp : Double
    -mHz : Double
    -mCount : Integer
    -mFilterWindow : Integer
    -mDataInit : Boolean
    -mDataLists : ArrayList
    -getMedian(List) : Double
}
@enduml
*/
package com.example.tsigg.falldetectionlib.filters;

import org.apache.commons.math3.stat.StatUtils;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by tsigg on 1/10/2016.
 */

public class MedianFilter implements Filter{
    private static final String TAG = MeanFilter.class.getSimpleName();

    private double timeConstant = 1;
    private double startTime = 0;
    private double timestamp = 0;
    private double hz = 0;

    private int count = 0;
    // The size of the mean filters rolling window.
    private int filterWindow = 20;

    private boolean dataInit;

    private ArrayList<LinkedList<Number>> dataLists;

    /**
     * Initialize a new MeanFilter object.
     */
    public MedianFilter()
    {
        dataLists = new ArrayList<LinkedList<Number>>();
        dataInit = false;
    }

    public void setTimeConstant(double timeConstant)
    {
        this.timeConstant = timeConstant;
    }

    public void reset()
    {
        startTime = 0;
        timestamp = 0;
        count = 0;
        hz = 0;
    }

    /**
     * Filter the data.
     *
     * @param data
     *            contains input the data.
     * @return the filtered output data.
     */
    @Override
    public double[] addSamples(double[] data)
    {
        // Initialize the start time.
        if (startTime == 0)
        {
            startTime = System.nanoTime();
        }

        timestamp = System.nanoTime();

        // Find the sample period (between updates) and convert from
        // nanoseconds to seconds. Note that the sensor delivery rates can
        // individually vary by a relatively large time frame, so we use an
        // averaging technique with the number of sensor updates to
        // determine the delivery rate.
        hz = (count++ / ((timestamp - startTime) / 1000000000.0));

        filterWindow = ((int) (hz * timeConstant)) > 0 ? (int) (hz * timeConstant) : 1;

        for (int i = 0; i < data.length; i++)
        {
            // Initialize the data structures for the data set.
            if (!dataInit)
            {
                dataLists.add(new LinkedList<Number>());
            }

            dataLists.get(i).addLast(data[i]);

            if (dataLists.get(i).size() > filterWindow)
            {
                dataLists.get(i).removeFirst();
            }
        }

        dataInit = true;

        double[] medians = new double[dataLists.size()];

        for (int i = 0; i < dataLists.size(); i++)
        {
            medians[i] = (float) getMedian(dataLists.get(i));
        }

        return medians;
    }

    /**
     * Get the mean of the data set.
     *
     * @param data
     *            the data set.
     * @return the mean of the data set.
     */
    private double getMedian(List<Number> data)
    {
        double[] values = new double[data.size()];

        for (int i = 0; i < values.length; i++)
        {
            values[i] = data.get(i).floatValue();
        }

        return StatUtils.percentile(values, 50);
    }
}

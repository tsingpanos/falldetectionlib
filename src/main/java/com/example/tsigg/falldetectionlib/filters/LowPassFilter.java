package com.example.tsigg.falldetectionlib.filters;

/**
 * Created by tsigg on 1/10/2016.
 */
/**
 * An implementation of the Android Developer low-pass filter. The Android
 * Developer LPF, is an IIR single-pole implementation. The coefficient, a
 * (alpha), can be adjusted based on the sample period of the sensor to produce
 * the desired time constant that the filter will act on. It is essentially the
 * same as the Wikipedia LPF. It takes a simple form of y[0] = alpha * y[0] + (1
 * - alpha) * x[0]. Alpha is defined as alpha = timeConstant / (timeConstant +
 * dt) where the time constant is the length of signals the filter should act on
 * and dt is the sample period (1/frequency) of the sensor.
 **/
public class LowPassFilter implements Filter{
    private static final String TAG = LowPassFilter.class.getSimpleName();

    // Constants for the low-pass filters
    private double timeConstant = 0.18;
    private double alpha = 0.9;
    private double dt = 0;

    // Timestamps for the low-pass filters
    private double timestamp = System.nanoTime();
    private double startTime = 0;

    private int count = 0;

    // Gravity and linear accelerations components for the
    // Wikipedia low-pass filter
    private double[] output = new double[] { 0, 0, 0 };

    // Raw accelerometer data
    private double[] input = new double[] { 0, 0, 0 };

    public LowPassFilter() {

    }

    /**
     * Add a sample.
     *
     * @param acceleration
     *            The mAcceleration data.
     * @return Returns the output of the filter.
     */
    @Override
    public double[] addSamples(double[] acceleration)
    {
        // Initialize the start time.
        if (startTime == 0)
        {
            startTime = System.nanoTime();
        }

        timestamp = System.nanoTime();

        // Get a local copy of the sensor values
        System.arraycopy(acceleration, 0, this.input, 0, acceleration.length);

        // Find the sample period (between updates) and convert from
        // nanoseconds to seconds. Note that the sensor delivery rates can
        // individually vary by a relatively large time frame, so we use an
        // averaging technique with the number of sensor updates to
        // determine the delivery rate.
        dt = 1 / (count++ / ((timestamp - startTime) / 1000000000.0));

        //alpha = timeConstant / (timeConstant + dt);
        alpha = dt / (timeConstant + dt);

        if (count > 5)
        {
            //output[0] = alpha * output[0] + (1 - alpha) * input[0];
            //output[1] = alpha * output[1] + (1 - alpha) * input[1];
            //output[2] = alpha * output[2] + (1 - alpha) * input[2];
            output[0] = (1 - alpha) * output[0] + alpha * input[0];
            output[1] = (1 - alpha) * output[1] + alpha * input[1];
            output[2] = (1 - alpha) * output[2] + alpha * input[2];
        }

        //return a copy of output so that it will not be modified outside
        double[] result = new double[3];
        System.arraycopy(output, 0, result, 0, output.length);

        return result;
    }

    public void setTimeConstant(double timeConstant)
    {
        this.timeConstant = timeConstant;
    }

    public void reset()
    {
        startTime = 0;
        timestamp = 0;
        count = 0;
        dt = 0;
        alpha = 0;
    }
}

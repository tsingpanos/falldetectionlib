package com.example.tsigg.falldetectionlib;

import android.app.IntentService;
import android.content.Intent;

import com.example.tsigg.falldetectionlib.classifiers.FallDetectionClassifierFactory;
import com.example.tsigg.falldetectionlib.classifiers.KnnClassifier;
import com.example.tsigg.falldetectionlib.fsm.InputBufferFSM;

import java.util.Arrays;
import java.util.Map;
import java.util.logging.Logger;


public class ClassifyService extends IntentService {

    private static final String TAG = ClassifyService.class.getSimpleName();
    private double mFeatures[];
    //private static MlpClassifier mClassifierMlp;
    private static KnnClassifier mClassifierKnn;
    //private static SvmClassifier mClassifierSvm;
    private Logger mLogger;

    public ClassifyService() {
        super("ClassifyService");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //if (mClassifierMlp == null)
        //    mClassifierMlp = (MlpClassifier) (new FallDetectionClassifierFactory(this).getClassifier(LibUtility.TAG_CLASSIFIER_MLP));
        if (mClassifierKnn == null)
            mClassifierKnn = (KnnClassifier) (new FallDetectionClassifierFactory(this).getClassifier(LibUtility.TAG_CLASSIFIER_KNN));
        //if (mClassifierSvm == null)
        //    mClassifierSvm = (SvmClassifier) (new FallDetectionClassifierFactory(this).getClassifier(LibUtility.TAG_CLASSIFIER_SVM));
        mLogger = LibUtility.getFallDetectionLogger(getApplicationContext());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (LibUtility.ACTION_CLASSIFY.equals(action)) {
                InputBufferFSM input = (InputBufferFSM) intent.getSerializableExtra(LibUtility.TAG_CLASSIFY_INPUT);
                double impactStartTime = intent.getDoubleExtra(LibUtility.TAG_CLASSIFY_IMPACT_START, 0);
                double impactEndTime = intent.getDoubleExtra(LibUtility.TAG_CLASSIFY_IMPACT_END, 0);
                double peakTime = intent.getDoubleExtra(LibUtility.TAG_CLASSIFY_PEAK, 0);
                double[] gravityVector = intent.getDoubleArrayExtra(LibUtility.TAG_CLASSIFY_GRAVITY);

                getFeatures(input, impactStartTime, impactEndTime, peakTime, gravityVector);
                //double out1 = mClassifierMlp.run(mFeatures);
                double out2 = mClassifierKnn.run(mFeatures);
                //double out3 = mClassifierSvm.run(mFeatures);

                /*
                Log.d(TAG, "Input size: " + input.getSize());
                Log.d(TAG, "Features:\n" +
                        mFeatures[0] + ", " + mFeatures[1] + ", " + mFeatures[2] + ", " + mFeatures[3] + ", " + mFeatures[4] + ", " + mFeatures[5] + ", " + mFeatures[6] + ", " + mFeatures[7] + ",\n" +
                        mFeatures[8] + ", " + mFeatures[9] + ", " + mFeatures[10] + ", " + mFeatures[11] + ", " + mFeatures[12] + ", " + mFeatures[13] + ", " + mFeatures[14] + ", " + mFeatures[15] + ", " + mFeatures[16] + ",\n" +
                        mFeatures[17] + ", " + mFeatures[18] + ", " + mFeatures[19] + ", " + mFeatures[20] + ", " + mFeatures[21] + ", " + mFeatures[22] + ",\n" +
                        mFeatures[23] + ", " + mFeatures[24]
                );
                */
                //Log.d(TAG, "MlpClassifier classifier output: " + out1);
                //Log.d(TAG, "KnnClassifier classifier output: " + out2);
                //Log.d(TAG, "SvmClassifier classifier output: " + out3);

                mLogger.fine("Input size: " + input.getSize());
                mLogger.fine(
                        "Features:\n" + Arrays.toString(mFeatures)
                );
                //mLogger.fine("MlpClassifier classifier output: " + out1);
                mLogger.fine("KnnClassifier classifier output: " + out2);
                //mLogger.fine("SvmClassifier classifier output: " + out3);

                if (out2 > 0.5) {
                    Intent fallIntent = new Intent();
                    fallIntent.setAction(LibUtility.ACTION_FALL);
                    fallIntent.putExtra(LibUtility.TAG_FEATURES, mFeatures);
                    sendBroadcast(fallIntent);
                    //Log.d(TAG, Utility.ACTION_FALL + " broadcast sent");
                    mLogger.fine(LibUtility.ACTION_FALL + " broadcast sent");
                }
            }
        }
    }

    private void getFeatures(InputBufferFSM input, double impactStartTime, double impactEndTime, double peakTime, double[] gravityVector) {
        double AAMV = FeatureExtractor.AAMV(input, impactStartTime, impactEndTime);
        double IDI = FeatureExtractor.IDI(impactStartTime, impactEndTime);
        double MPI = FeatureExtractor.MPI(input, impactStartTime, impactEndTime);
        //double MVI = FeatureExtractor.MVI(input, impactStartTime, impactEndTime);
        double PDI = FeatureExtractor.PDI(input, peakTime);
        double FFI = FeatureExtractor.FFI(input, peakTime);
        double ARI = FeatureExtractor.ARI(input, impactStartTime, impactEndTime);
        double SCI = FeatureExtractor.SCI(input, peakTime);

        double IMPACT_POWER = FeatureExtractor.IMPACT_POWER(input, impactStartTime, impactEndTime);
        double IMPACT_STD = FeatureExtractor.IMPACT_STD(input, impactStartTime, impactEndTime);
        Map STATS = FeatureExtractor.STATISTICS(input);
        //double POWER = (double)STATS.get(FeatureExtractor.STATS_MAP_POWER);
        //double STD = (double)STATS.get(FeatureExtractor.STATS_MAP_STD);
        //double MEAN = (double)STATS.get(FeatureExtractor.STATS_MAP_MEAN);
        double SKEW = (double)STATS.get(FeatureExtractor.STATS_MAP_SKEWNESS);
        double KURT = (double)STATS.get(FeatureExtractor.STATS_MAP_KURTOSIS);
        //double MEDIAN = (double)STATS.get(FeatureExtractor.STATS_MAP_MEDIAN);
        double IQR = (double)STATS.get(FeatureExtractor.STATS_MAP_IQR);
        //double RMS = (double)STATS.get(FeatureExtractor.STATS_MAP_RMS);
        //double DEVMEAN = (double)STATS.get(FeatureExtractor.STATS_MAP_DEVMEAN);

        Map CWT = FeatureExtractor.CWT(input);  // cwt
        //double MAX_ENERGY = (double)CWT.get(FeatureExtractor.CWT_MAP_MAX_ENERGY);
        double TOTAL_ENERGY = (double)CWT.get(FeatureExtractor.CWT_MAP_TOTAL_ENERGY);
        //double ENERGY_RATIO = (double)CWT.get(FeatureExtractor.CWT_MAP_ENERGY_RATIO);
        int PEAKS = (int)CWT.get(FeatureExtractor.CWT_MAP_PEAKS);
        //double VECTOR_ANGLE = FeatureExtractor.VECTOR_ANGLE(input, gravityVector, impactEndTime);
        //double VVT = FeatureExtractor.VVT(input, gravityVector);

        mFeatures = new double[]{
                AAMV, IDI, MPI, PDI, ARI, FFI, SCI,
                SKEW, KURT, IQR,
                IMPACT_POWER, IMPACT_STD, TOTAL_ENERGY, PEAKS
        };
    }
}

package com.example.tsigg.falldetectionlib;

import android.content.Context;
import android.os.Environment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

/**
 * Created by tsigg on 2/1/2017.
 */

public class LibUtility {

    public static final String ACTION_FSM_FALL = "com.example.tsigg.falldetection.action.ACTION_FSM_FALL";
    public static final String ACTION_CLASSIFY = "com.example.tsigg.falldetection.action.ACTION_CLASSIFY";
    public static final String ACTION_FALL = "com.example.tsigg.falldetection.action.ACTION_FALL";

    public static final String FOLDER_CLASSIFIER = "Classifier";
    public static final String FILE_TRAIN_DATA = "TrainingData.csv";
    public static final String TAG_CLASSIFIER_KNN = "com.example.tsigg.falldetection.tag.KNNCLASSIFIER";
    public static final String TAG_CLASSIFIER_MLP = "com.example.tsigg.falldetection.tag.MLPCLASSIFIER";
    public static final String TAG_CLASSIFIER_SVM = "com.example.tsigg.falldetection.tag.SVMCLASSIFIER";
    public static final String TAG_CLASSIFIER_VOTE = "com.example.tsigg.falldetection.tag.VOTECLASSIFIER";
    public static final String TAG_FEATURES = "com.example.tsigg.falldetection.tag.FEATURES";
    public static final String CLASS_FALL = "FALL";
    public static final String CLASS_ADL = "ADL";
    public static final String FILE_MODEL_KNN = "KNNClassifier.model";
    public static final String FILE_MODEL_MLP = "MLPClassifier.model";
    public static final String FILE_MODEL_SVM = "SVMClassifier.model";
    public static final String FILE_MODEL_VOTE = "VoteClassifier.model";

    public static final String FOLDER_LOG = "Log";
    public static final String FILE_LOG = "fallDetection.log";
    public static final String TAG_CLASSIFY_INPUT = "com.example.tsigg.falldetection.tag.INPUT";
    public static final String TAG_CLASSIFY_IMPACT_START = "com.example.tsigg.falldetection.tag.IMPACT_START";
    public static final String TAG_CLASSIFY_IMPACT_END = "com.example.tsigg.falldetection.tag.IMPACT_END";
    public static final String TAG_CLASSIFY_PEAK = "com.example.tsigg.falldetection.tag.PEAK";
    public static final String TAG_CLASSIFY_GRAVITY = "com.example.tsigg.falldetection.tag.GRAVITY";


    private static String mPath = null;
    private static Logger mLogger = null;

    public static Logger getFallDetectionLogger(Context context) {
        if (mLogger == null)
            mLogger = initLogger(context);
        return mLogger;
    }

    private static Logger initLogger(Context context) {

        Logger logger = Logger.getLogger("com.example.tsigg.falldetection.logger");
        String fileName = getExternalStorageDirectory(context) + File.separator + FOLDER_LOG + File.separator + FILE_LOG;

        try {
            boolean append = true;
            int limit = 1024*1024;
            String pattern = fileName;
            int numLogFiles = 5;
            FileHandler handler = new FileHandler(pattern, limit, numLogFiles, append);

            Formatter formatter = new Formatter() {
                public String format(LogRecord record) {
                    String recordStr = "{Date} " + new Date() + " {Log Level} "
                            + record.getLevel() + " {Class} "
                            + record.getSourceClassName() + " {Method} "
                            + record.getSourceMethodName() + " {Message} "
                            + record.getMessage() + "\n";
                    return recordStr;
                }
            };
            if (formatter != null)
                handler.setFormatter(formatter);

            logger.addHandler(handler);
        } catch (IOException e) {
            e.printStackTrace();
        }
        logger.setLevel(Level.ALL);

        return logger;
    }

    public static String getExternalStorageDirectory(Context context) {
        if (mPath == null)
            mPath = Environment.getExternalStorageDirectory() + File.separator + context.getString(R.string.app_name).replaceAll(" ","");
        return mPath;
    }


    public static void assetToFile(Context context, String assetFilename, String path) {
        try {
            InputStream inputStream = context.getResources().getAssets().open(assetFilename);
            FileOutputStream outputStream = new FileOutputStream(path, false);
            int read;
            byte[] buffer = new byte[1024];
            while ((read = inputStream.read(buffer)) != -1)
                outputStream.write(buffer, 0 ,read);
            inputStream.close();
            inputStream = null;
            outputStream.flush();
            outputStream.close();
            outputStream = null;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

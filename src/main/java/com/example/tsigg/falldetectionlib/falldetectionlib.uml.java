/*
@startuml

namespace falldetectionlib {
    namespace classifiers {
    }
    namespace filters {
    }
    namespace fsm {
    }

    class ClassifyService
    class CustomSensorEventListener
    class FeatureExtractor
    class LibUtility

}

@enduml
*/
////////////////////////////////////////////////////////////////////////////////////////////////////

/*
@startuml

class ClassifyService extends IntentService {
    -mFeatures : Double[]
    -{static}mKNN : KnnClassifier
    -{static}mMLP : MlpClassifier
    -{static}mSVM : SvmClassifier
    +onCreate()
    #onHandleIntent(Intent)
    -getFeatures()
}

@enduml
*/
////////////////////////////////////////////////////////////////////////////////////////////////////

/*
@startuml

class FeatureExtractor {
    -{static}YWAV : Double[]
    -{static}XWAVSTEP : Double
    +{static}AAMV(InputBuffer, Double, Double) : Double
    +{static}IDI(Double, Double) : Double
    +{static}MPI(InputBuffer, Double, Double) : Double
    +{static}MVI(InputBuffer, Double, Double) : Double
    +{static}PDI(InputBuffer, Double) : Double
    +{static}FFI(InputBuffer, Double) : Double
    +{static}ARI(InputBuffer, Double, Double) : Double
    +{static}SCI(InputBuffer, Double) : Double
    +{static}IMPACT_POWER(InputBuffer, Double, Double) : Double
    +{static}IMPACT_STD(InputBuffer, Double, Double) : Double
    +{static}CWT(InputBuffer) : Double[]
    +{static}VECTOR_ANGLE(InputBuffer, Double[], Double) : Double
    +{static}VVT(InputBuffer, Double[]) : Double
    +{static}STATISTICS(InputBuffer) : Double[]
}

@enduml
*/
////////////////////////////////////////////////////////////////////////////////////////////////////

/*
@startuml

class LibUtility {
    +{static}ACTION_FSM_FALL : String
    +{static}ACTION_CLASSIFY : String
    +{static}ACTION_FALL : String
    +{static}FOLDER_CLASSIFIER : String
    +{static}FILE_TRAIN_DATA : String
    +{static}TAG_CLASSIFIER_KNN : String
    +{static}TAG_CLASSIFIER_MLP : String
    +{static}TAG_CLASSIFIER_SVM : String
    +{static}TAG_CLASSIFIER_VOTE : String
    +{static}TAG_FEATURES : String
    +{static}CLASS_FALL : String
    +{static}CLASS_ADL  : String
    +{static}FILE_MODEL_KNN : String
    +{static}FILE_MODEL_MLP : String
    +{static}FILE_MODEL_SVM : String
    +{static}FILE_MODEL_VOTE : String
    +{static}FOLDER_LOG : String
    +{static}FILE_LOG : String
    +{static}TAG_CLASSIFY_INPUT : String
    +{static}TAG_CLASSIFY_IMPACT_START : String
    +{static}TAG_CLASSIFY_IMPACT_END : String
    +{static}TAG_CLASSIFY_PEAK : String
    +{static}TAG_CLASSIFY_GRAVITY : String
    -{static}mPath : String
    -{static}mLogger : Logger
    +{static}getFallDetectionLogger(Context) : Logger
    +{static}getExternalStorageDirectory(Context) : String
    +{static}assetToFile(Context, String, String)
}

@enduml
 */
package com.example.tsigg.falldetectionlib;

import com.example.tsigg.falldetectionlib.fsm.InputBufferFSM;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.stat.StatUtils;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.apache.commons.math3.stat.descriptive.moment.Kurtosis;
import org.apache.commons.math3.stat.descriptive.moment.Skewness;
import org.apache.commons.math3.util.MathArrays;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static java.lang.Double.NaN;

/**
 * Created by tsigg on 19/10/2016.
 */

public final class FeatureExtractor {

    public static final String STATS_MAP_MEAN = "STATS_MAP_MEAN";
    public static final String STATS_MAP_VARIANCE = "STATS_MAP_VARIANCE";
    public static final String STATS_MAP_STD = "STATS_MAP_STD";
    public static final String STATS_MAP_MEDIAN = "STATS_MAP_MEDIAN";
    public static final String STATS_MAP_SKEWNESS= "STATS_MAP_SKEWNESS";
    public static final String STATS_MAP_KURTOSIS = "STATS_MAP_KURTOSIS";
    public static final String STATS_MAP_POWER = "STATS_MAP_POWER";
    public static final String STATS_MAP_IQR = "STATS_MAP_IQR";
    public static final String STATS_MAP_RMS = "STATS_MAP_RMS";
    public static final String STATS_MAP_DEVMEAN = "STATS_MAP_DEVMEAN";

    public static final String CWT_MAP_MAX_ENERGY = "CWT_MAP_MAX_ENERGY";
    public static final String CWT_MAP_TOTAL_ENERGY = "CWT_MAP_TOTAL_ENERGY";
    public static final String CWT_MAP_ENERGY_RATIO = "CWT_MAP_ENERGY_RATIO";
    public static final String CWT_MAP_PEAKS = "CWT_MAP_PEAKS";

    private static final double[] YWAV = new double[]{
            0,-0.00020144,-0.00068111,-0.0014739,-0.0026377,-0.0039712,-0.0055128,-0.0068801,-0.0087241,-0.010832,-0.013282,-0.01608,-0.018989,-0.022381,-0.026393,-0.030683,-0.035166,-0.040241,-0.045285,-0.050905,-0.056961,-0.062683,-0.068238,-0.074049,-0.079727,-0.086283,-0.0925,-0.098948,-0.10607,-0.11323,-0.12034,-0.12801,-0.13613,-0.14425,-0.15184,-0.15897,-0.16587,-0.17272,-0.17884,-0.18371,-0.18805,-0.19069,-0.1929,-0.19489,-0.19613,-0.19637,-0.19417,-0.17922,-0.13414,-0.068699,-0.024764,-0.011889,-0.012394,-0.01604,-0.022411,-0.029588,-0.035438,-0.03657,-0.034147,-0.027867,-0.021329,-0.017858,-0.015608,-0.01496,-0.016504,-0.019365,-0.022077,-0.024798,-0.027514,-0.030126,-0.032809,-0.034892,-0.036764,-0.039072,-0.041215,-0.043036,-0.044495,-0.045804,-0.047284,-0.048563,-0.050128,-0.051711,-0.053469,-0.054906,-0.056171,-0.057253,-0.058143,-0.05901,-0.060002,-0.061033,-0.062003,-0.062695,-0.063403,-0.064028,-0.06443,-0.06481,-0.065164,-0.06534,-0.065493,-0.065493
    };
    private static final double XWAVSTEP = 1.0 / (YWAV.length-1);

    private FeatureExtractor() {

    }

    public static double AAMV(InputBufferFSM input, double impactStartTime, double impactEndTime) {
        return AAMV(input, impactStartTime, impactEndTime, 1000);
    }

    public static double AAMV(InputBufferFSM input, double impactStartTime, double impactEndTime, double win) {
        double aamv = 0;

        try {
            double[] a;
            double[] t = input.getTime();
            if (win != impactEndTime-impactStartTime) {
                double i = (impactStartTime+impactEndTime)/2.0f - win/2.0f;
                double j = (impactStartTime+impactEndTime)/2.0f + win/2.0f;
                a = input.getMagnitudeRange(i, j);
            } else
                a = input.getMagnitudeRange(impactStartTime, impactEndTime);

            double[] d = MathUtilities.diff(a, true);
            aamv = StatUtils.mean(d);
        } catch (Exception e) {
            e.printStackTrace();
            aamv = NaN;
        }

        return aamv;
    }

    public static double IDI(double impactStartTime, double impactEndTime) {
        double idi;

        try {
            idi = impactEndTime - impactStartTime;
        } catch (Exception e) {
            e.printStackTrace();
            idi = NaN;
        }

        return idi;
    }

    public static double MPI(InputBufferFSM input, double impactStartTime, double impactEndTime) {
        double mpi;

        try {
            double[] a = input.getMagnitudeRange(impactStartTime, impactEndTime);
            mpi = StatUtils.max(a);
        } catch (Exception e) {
            e.printStackTrace();
            mpi = NaN;
        }

        return mpi;
    }

    public static double MVI(InputBufferFSM input, double impactStartTime, double impactEndTime) {
        double mvi;

        try {
            double[] a = input.getMagnitudeRange(impactStartTime - 500, impactEndTime);
            mvi = StatUtils.min(a);
        } catch (Exception e) {
            e.printStackTrace();
            mvi = NaN;
        }

        return mvi;
    }

    public static double PDI(InputBufferFSM input, double peakTime) {
        return PDI(input, peakTime,  1.8f);
    }

    public static double PDI(InputBufferFSM input, double peakTime, double threshold) {
        double pdi = 0;

        try {
            int p = input.indexOfTime(peakTime);
            int k, l;
            double[] a = input.getMagnitudeRange(0, p - 1);
            // Last below 1.8g before peak
            for (k = a.length - 1; k >= 0; k--) {
                if (a[k] < threshold)
                    break;
            }

            // Initially I got: a = input.getMagnitudeRange(p, input.getSize() - 1);
            a = input.getMagnitudeRange(p+1, input.getSize() - 1);
            // First below 1.8g after peak
            for (l = 0; l <= a.length; l++) {
                if (a[l] < threshold)
                    break;
            }

            l = l + p;
            pdi = input.get(l).getTime() - input.get(k).getTime();
        } catch (Exception e) {
            e.printStackTrace();
            pdi = NaN;
        }

        return pdi;
    }

    public static double FFI(InputBufferFSM input, double peakTime) {
        double ffi = 0;

        try {
            int p = input.indexOfTime(peakTime);
            int k = input.indexOfTime(peakTime - 200);
            double[] acc = input.getMagnitudeRange(k, p);
            int l = indexOf(acc, 0.8f, true, false);
            double t = input.get(k + l).getTime();
            double[] time = input.getTime();
            int m = indexOf(time, t - 200, false, true);

            double a[] = input.getMagnitudeRange(m, k + l);
            ffi = StatUtils.mean(a);
        } catch (Exception e) {
            e.printStackTrace();
            ffi = NaN;
        }

        return ffi;
    }

    public static double ARI(InputBufferFSM input, double impactStartTime, double impactEndTime) {
        return ARI(input, impactStartTime, impactEndTime, 700f, 1.3f, 0.8f);
    }

    public static double ARI(InputBufferFSM input, double impactStartTime, double impactEndTime, double win, double thresholdUp, double thresholdDown) {
        double ari = 0;

        try {
            double[] a;
            if (win != (impactEndTime - impactStartTime)) {
                double i = (impactStartTime + impactEndTime) / 2.0f - win / 2.0f;
                double j = (impactStartTime + impactEndTime) / 2.0f + win / 2.0f;
                a = input.getMagnitudeRange(i, j);
            } else {
                a = input.getMagnitudeRange(impactStartTime, impactEndTime);
            }

            double[] above = getAbove(a, thresholdUp);
            double[] below = getBelow(a, thresholdDown);
            ari = (double) (above.length + below.length) / (double) a.length;
        } catch (Exception e) {
            e.printStackTrace();
            ari = NaN;
        }

        return ari;
    }

    public static double SCI(InputBufferFSM input, double peakTime) {
        double sci;

        try {
            sci = SCI(input, peakTime, 2200f);
        } catch (Exception e) {
            e.printStackTrace();
            sci = NaN;
        }

        return sci;
    }

    public static double SCI(InputBufferFSM input, double peakTime, double win) {
        double sci = 0;

        try {
            double[] a = input.getMagnitudeRange(peakTime - win, peakTime);
            double[] t = input.getTimeRange(peakTime - win, peakTime);

            // Below 1g, Above 1.5g
            ArrayList<Integer> below = new ArrayList<>(1);
            ArrayList<Integer> above = new ArrayList<>(1);
            for (int i = 0; i < a.length; i++) {
                if (a[i] < 1.0) {
                    below.add(i);
                } else if (a[i] > 1.2) {
                    above.add(i);
                }
            }

            // Consecutive below 1g
            int[] d = new int[below.size()];
            for (int i = 0; i < below.size() - 1; i++) {
                d[i] = (below.get(i + 1) - below.get(i)) == 1 ? 1 : 0;
            }
            // Consecutive start-end
            ArrayList<Integer[]> con = new ArrayList<>(1);
            {
                int i = 0;
                while (i < d.length) {
                    if (d[i] == 1) {
                        int j = i;
                        for (j = i; j < d.length; j++)
                            if (d[j] != 1) break;
                        con.add(new Integer[]{below.get(i), below.get(j)});
                        i = j;
                    } else
                        i++;
                }
            }
            // Possible valleys
            ArrayList<Integer[]> pval1 = new ArrayList<>(1);
            for (int i = 0; i < con.size(); i++) {
                if (t[con.get(i)[1]] - t[con.get(i)[0]] >= 80)
                    pval1.add(con.get(i));
            }

            // Possible valleys with peak
            ArrayList<Integer[]> pval2 = new ArrayList<>(1);
            for (int i = 0; i < pval1.size(); i++) {
                for (int j = 0; j < above.size(); j++) {
                    double temp = t[above.get(j)] - t[pval1.get(i)[1]];
                    if (temp > 0 && temp <= 200) {
                        pval2.add(pval1.get(i));
                        above.remove(j);
                        break;
                    }
                }
            }

            // Valleys
            ArrayList<Integer[]> val = new ArrayList<>(1);
            for (int i = 0; i < pval2.size() - 1; i++) {
                if (t[pval2.get(i + 1)[0]] - t[pval2.get(i)[1]] >= 200)
                    val.add(pval2.get(i));
            }

            sci = val.size()+1;
//            if (sci == 0 && pval2.size() == 1)
//                sci++;
        } catch (Exception e) {
            e.printStackTrace();
            sci = NaN;
        }

        return sci;
    }

    public static double POWER(InputBufferFSM input) {
        double power;

        try {
            double[] m = input.getMagnitude();
            power = StatUtils.mean(MathUtilities.pow(m, 2));
        } catch (Exception e) {
            e.printStackTrace();
            power = NaN;
        }

        return power;
    }

    public static double STD(InputBufferFSM input) {
        double std;

        try {
            std = Math.sqrt(StatUtils.variance(input.getMagnitude()));
        } catch (Exception e) {
            e.printStackTrace();
            std = NaN;
        }

        return std;
    }

    public static double MEAN(InputBufferFSM input) {
        double mm;

        try {
            double[] m = input.getMagnitude();
            mm = StatUtils.mean(m);
        } catch (Exception e) {
            e.printStackTrace();
            mm = NaN;
        }

        return mm;
    }

    public static double SKEW(InputBufferFSM input) {
        double skew;
        Skewness sk = new Skewness();
        try {
            double[] m = input.getMagnitude();
            skew = sk.evaluate(m, 0, m.length-1);
            //double m1 = mean(m);
            //double m2 = StatUtils.variance(m, m1);
            //double num = StatUtils.sumSq(
            //        MathUtilities.sub(m, m1)
            //) / m.length;
            //double den = m2 * Math.sqrt(m2);
            //skew = num / den;
        } catch (Exception e) {
            e.printStackTrace();
            skew = NaN;
        }
        
        return skew;
    }

    public static double KURT(InputBufferFSM input) {
        double kurt;
        Kurtosis kurtosis = new Kurtosis();
        
        try {
            double[] m = input.getMagnitude();
            kurt = kurtosis.evaluate(m, 0, m.length-1);
            //double m1 = mean(m);
            //double num = StatUtils.mean(
            //        MathUtilities.pow(
            //                MathUtilities.sub(m, m1),
            //                4)
            //);
            //double den = Math.pow(
            //        StatUtils.variance(m, m1), 2);
            //kurt = num / den;
        } catch (Exception e) {
            e.printStackTrace();
            kurt = NaN;
        }
        
        return kurt;
    }

    public static double IMPACT_POWER(InputBufferFSM input, double impactStartTime, double impactEndTime) {
        double power;
        
        try {
            double[] m = input.getMagnitudeRange(impactStartTime, impactEndTime);
            power = StatUtils.mean(MathUtilities.pow(m, 2));
        } catch (Exception e) {
            e.printStackTrace();
            power = NaN;
        }
        
        return power;
    }

    public static double IMPACT_STD(InputBufferFSM input, double impactStartTime, double impactEndTime) {
        double std;
        
        try {
            double[] m = input.getMagnitudeRange(impactStartTime, impactEndTime);
            std = Math.sqrt(StatUtils.variance(m));
        } catch (Exception e) {
            e.printStackTrace();
            std = NaN;
        }
        return std;
    }

    public static Map COR(InputBufferFSM input) {
        double maxEnergy, totalEnergy, energyRatio, p;
        int numPeaks;
        Map res = new HashMap(4);

        try {
            double[] m = input.getMagnitude();
            double[] cwt = MathArrays.convolve(m, YWAV);

            p = StatUtils.max(cwt);
            maxEnergy = p * p / cwt.length;
            totalEnergy = StatUtils.sumSq(cwt) / cwt.length;
            energyRatio = maxEnergy / totalEnergy;
            numPeaks = findPeaks(cwt, 0.7 * p);
        } catch (Exception e) {
            e.printStackTrace();
            maxEnergy = NaN;
            totalEnergy = NaN;
            energyRatio = NaN;
            numPeaks = -1;
        }

        res.put("maxEnergy", maxEnergy );
        res.put("totalEnergy", totalEnergy);
        res.put("energyRatio", energyRatio);
        res.put("numPeaks", numPeaks);

        return res;
    }

    public static Map CWT(InputBufferFSM input) {
        double maxEnergy, totalEnergy, energyRatio, p=0;
        int numPeaks, pi=0;
        Map res = new HashMap(4);

        try {
            int scales = 2;
            double[] mag = input.getMagnitude();
            final int cwt_length = mag.length + scales - 1;
            final int off05 = (int)Math.ceil(0.1*cwt_length);
            final int cwt_length2 = cwt_length - 2*off05;

            ArrayList<double[]> cwt = new ArrayList<>(scales);
            for (int i = 1; i <= scales; i++) {
                int[] J = new int[i+1];
                for (int j = 0; j <= i; j++)
                    J[j] = (int) ( ((double)j) / (i*XWAVSTEP));
                double[] W = new double[J.length];
                for (int j = 0; j < J.length; j++)
                    W[W.length - j - 1] = -Math.sqrt(i)*YWAV[J[j]];            // scaled wavelet
                double[] C = MathArrays.convolve(mag, W);             // length: cwt_length
                C = MathUtilities.diff(C, false);
                double[] cwt_row = new double[cwt_length2];
                int off = C.length / 2 - cwt_length / 2;        // central part
                for (int j = 0; j < cwt_length2; j++) {
                    cwt_row[j] = C[off + off05 + j];                      // keep central part of convolution
                }
                cwt.add(cwt_row);

                double k = StatUtils.max(cwt_row);    // Find max
                if (k > p) {
                    p = k;
                    pi = indexOf(cwt_row, k, false, true);
                }
            }

            maxEnergy = 0;
            totalEnergy = 0;
            double[] cwt_single = new double[cwt_length2];
            for (int i = 0; i < scales; i++) {
                double[] row = cwt.get(i);
                double k = row[pi];
                maxEnergy += k * k;
                totalEnergy += StatUtils.sumSq(row);
                for (int j = 0; j < cwt_length2; j++) {
                    cwt_single[j] += row[j];
                }
            }
            maxEnergy /= (p*p*scales);
            totalEnergy /= (p*p*scales);
            energyRatio = maxEnergy / totalEnergy;

            double m = StatUtils.min(cwt_single);
            cwt_single = MathUtilities.sub(cwt_single, m);
            p = StatUtils.max(cwt_single);
            numPeaks = findPeaks(cwt_single, 0.7 * p);
        } catch (Exception e) {
            e.printStackTrace();
            maxEnergy = NaN;
            totalEnergy = NaN;
            energyRatio = NaN;
            numPeaks = -1;
        }

        res.put(CWT_MAP_MAX_ENERGY, maxEnergy);
        res.put(CWT_MAP_TOTAL_ENERGY, totalEnergy);
        res.put(CWT_MAP_ENERGY_RATIO, energyRatio);
        res.put(CWT_MAP_PEAKS, numPeaks);

        return res;
    }

    public static double VECTOR_ANGLE(InputBufferFSM input, double[] g, double impactEndTime) {
        double angle = NaN;

        try {
            double[] x = input.getXAxisRange(impactEndTime, true);
            double[] y = input.getYAxisRange(impactEndTime, true);
            double[] z = input.getZAxisRange(impactEndTime, true);
            double[] d = new double[]{StatUtils.percentile(x, 50), StatUtils.percentile(y, 50), StatUtils.percentile(z, 50)};

            assert g.length == 3;
            assert d.length == 3;
            Vector3D vG = new Vector3D(g);
            Vector3D vD = new Vector3D(d);
            angle = Vector3D.angle(vG, vD);
        } catch (Exception e) {
            e.printStackTrace();
            angle = NaN;
        }

        return angle;
    }

    public static double VVT(InputBufferFSM input,  double[] g) {
        double vvt = 0;

        try {
            double[] m = input.getMagnitude();
            double G = Math.sqrt(g[0]*g[0]+g[1]*g[1]+g[2]*g[2]);
            double[] mm = MathUtilities.sub(m, G);

            // Trapezoidal Rule
            for (int i=1; i<mm.length-1; i++) {
                vvt += 2*mm[i];
            }
            vvt += (mm[0] + mm[mm.length-1]);
            vvt /= 2;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return vvt;
    }

    public static Map STATISTICS(InputBufferFSM input) {
        double _mean, _variance, _skewness, _kurtosis, _sumsq, _std, _p25, _p50, _p75, _rms, _mdev;

        try {
            double[] m = input.getMagnitude();
            DescriptiveStatistics descriptiveStatistics = new DescriptiveStatistics(m);

            _mean = descriptiveStatistics.getMean();
            _variance = descriptiveStatistics.getVariance();
            _skewness = descriptiveStatistics.getSkewness();
            _kurtosis = descriptiveStatistics.getKurtosis();
            _sumsq = descriptiveStatistics.getSumsq() / m.length;
            _std = descriptiveStatistics.getStandardDeviation();
            _p25 = descriptiveStatistics.getPercentile(25);
            _p50 = descriptiveStatistics.getPercentile(50);
            _p75 = descriptiveStatistics.getPercentile(75);
            _rms = Math.sqrt(_sumsq);
            _mdev = StatUtils.mean(MathUtilities.diff(m, false));
        } catch (Exception e) {
            e.printStackTrace();
            _mean = NaN;
            _variance = NaN;
            _std = NaN;
            _skewness = NaN;
            _kurtosis = NaN;
            _sumsq = NaN;
            _p25 = NaN;
            _p50 = NaN;
            _p75 = NaN;
            _rms = NaN;
            _mdev = NaN;
        }

        Map res = new HashMap(8);
        res.put(STATS_MAP_MEAN, _mean);
        res.put(STATS_MAP_VARIANCE, _variance);
        res.put(STATS_MAP_STD, _std);
        res.put(STATS_MAP_SKEWNESS, _skewness);
        res.put(STATS_MAP_KURTOSIS, _kurtosis);
        res.put(STATS_MAP_POWER, _sumsq);
        res.put(STATS_MAP_MEDIAN, _p50);
        res.put(STATS_MAP_IQR, _p75 - _p25);
        res.put(STATS_MAP_RMS, _rms);
        res.put(STATS_MAP_DEVMEAN, _mdev);
        return res;
    }

    private static double[] getAbove(double[] a, double thr) {
        double[] ab;
        ArrayList<Integer> abi = new ArrayList<>(5);

        for (int i=0; i<a.length; i++) {
            if (a[i] > thr)
                abi.add(i);
        }
        ab = new double[abi.size()];
        for (int i=0; i<abi.size(); i++) {
            ab[i] = a[abi.get(i)];
        }

        return ab;
    }

    private static double[] getBelow(double[] a, double thr) {
        double[] be;
        ArrayList<Integer> bei = new ArrayList<>(5);

        for (int i=0; i<a.length; i++) {
            if (a[i] < thr)
                bei.add(i);
        }
        be = new double[bei.size()];
        for (int i=0; i<bei.size(); i++) {
            be[i] = a[bei.get(i)];
        }

        return be;
    }

    private static int findPeaks(double[] a, double thr) {
        assert a.length > 3;
        int numPeaks = 0, k=-10;

        for (int i=1; i<a.length-1; i++)
            if (a[i] > thr &&       // peak above threshold
                    i-k >= 10 &&    // min distance between peaks
                    a[i] > a[i-1] && a[i] > a[i+1]) {
                numPeaks++;
                k = i;
            }

        return numPeaks;
    }

    private static int indexOf(double[] array, double value, boolean revDir, boolean gr) {
        int res = 0;
        if (!revDir && gr) {
            for (int i = 0; i < array.length; i++)
                //if (Precision.equals(array[i], value, 5.0f)) {
                if (array[i] - value >= 0) {
                    res = i;
                    break;
                }
        } else if (!revDir && !gr) {
            for (int i = 0; i < array.length; i++)
                //if (Precision.equals(array[i], value, 5.0f)) {
                if (array[i] - value <= 0) {
                    res = i;
                    break;
                }
        } else if (revDir && gr) {
            for (int i = array.length-1; i >= 0; i--)
                //if (Precision.equals(array[i], value, 5.0f)) {
                if (array[i] - value >= 0) {
                    res = i;
                    break;
                }
        } else if (revDir && !gr) {
            for (int i = array.length-1; i >= 0; i--)
                //if (Precision.equals(array[i], value, 5.0f)) {
                if (array[i] - value <= 0) {
                    res = i;
                    break;
                }
        }
        return res;
    }
}

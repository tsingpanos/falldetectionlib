package com.example.tsigg.falldetectionlib;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import com.example.tsigg.falldetectionlib.filters.Filter;

import java.util.concurrent.BlockingQueue;

/**
 * Created by tsigg on 2/1/2017.
 */

public class CustomSensorEventListener implements SensorEventListener {
    private static final String TAG = CustomSensorEventListener.class.getSimpleName();
    private Filter mFilter;
    private double[] mAcceleration = new double[3];
    private double mStartTime = 0;
    private double mTimestamp = 0;
    private double mHz;
    private long mCount;
    private volatile BlockingQueue[] mDataQueues;
    private double[] mError;

    public CustomSensorEventListener(Filter filter, double[] error, BlockingQueue[]queues) {
        mDataQueues = queues;
        mFilter = filter;
        mError = error;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        //Log.d(TAG, "Accuracy for sensor " + sensor.getName() + " changed to " + accuracy);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            if (mStartTime == 0)
                mStartTime = System.nanoTime();
            double time = System.nanoTime();
            mTimestamp = (time - mStartTime) / 1000000.0f;
            calculateSensorFrequency(time);

            //Log.d(TAG, "Raw values" + Arrays.toString(event.values));
            for (int i = 0; i < 3; i++)
                mAcceleration[i] = (double) event.values[i] + mError[i];

            //Log.d(TAG, "Calibrated values" + Arrays.toString(mAcceleration));
            mAcceleration = mFilter.addSamples(mAcceleration);
            //Log.d(TAG, "Filtered values" + Arrays.toString(mAcceleration));

            double data[] = new double[]{mTimestamp, mHz, mAcceleration[0] / SensorManager.GRAVITY_EARTH,
                    mAcceleration[1] / SensorManager.GRAVITY_EARTH,
                    mAcceleration[2] / SensorManager.GRAVITY_EARTH};
            try {
                for (int i=0;i<mDataQueues.length;i++)
                    mDataQueues[i].add(data);
            } catch (Exception e) {
                e.printStackTrace();
            }
            //Log.d(TAG, "Acceleration: " + mAcceleration[0] + ", " + mAcceleration[1] + ", " + mAcceleration[2]);
        }

    }

    private void calculateSensorFrequency(double time) {
        // Initialize the start time.
        if (mStartTime == 0) {
            mStartTime = System.nanoTime();
        }

        //mTimestamp = System.nanoTime();

        // Find the sample period (between updates) and convert from
        // nanoseconds to seconds. Note that the sensor delivery rates can
        // individually vary by a relatively large time frame, so we use an
        // averaging technique with the number of sensor updates to
        // determine the delivery rate.
        mHz = (mCount++ / ((time - mStartTime) / 1000000000.0f));
        mTimestamp = (time - mStartTime) / 1000000.0f;

        //Log.d(TAG, "Frequency calculated: "+ mHz);
    }

    public void reset() {
        mFilter = null;
        mStartTime = 0;
        mTimestamp = 0;
        mHz = 0;
        mCount = 0;
    }
}

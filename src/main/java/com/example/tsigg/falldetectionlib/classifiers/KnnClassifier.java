package com.example.tsigg.falldetectionlib.classifiers;

import android.content.Context;

import com.example.tsigg.falldetectionlib.LibUtility;

import java.io.File;
import java.util.Arrays;

import weka.classifiers.lazy.IBk;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.ManhattanDistance;
import weka.core.SelectedTag;
import weka.core.SerializationHelper;
import weka.core.neighboursearch.LinearNNSearch;

/**
 * Created by tsigg on 31/10/2016.
 */

public class KnnClassifier implements FallDetectionClassifier {

    private static final String TAG = KnnClassifier.class.getSimpleName();
    private IBk mKnn;
    private Context mContext;
    private String mPath;
    private static KnnClassifier mKNNInstance;

    public static KnnClassifier getKNNInstance(Context context) {
        if (mKNNInstance == null)
            mKNNInstance = new KnnClassifier(context);
        return mKNNInstance;
    }

    private KnnClassifier(Context context) {
        mContext = context.getApplicationContext();
        mPath = LibUtility.getExternalStorageDirectory(mContext);
        init();
    }

    @Override
    public void init() {
        String classifierPath = mPath + File.separator + LibUtility.FOLDER_CLASSIFIER + File.separator + LibUtility.FILE_MODEL_KNN;

        File file = new File(classifierPath);
        if (file.exists() && !file.isDirectory()) {
            try {
                mKnn = (IBk) SerializationHelper.read(classifierPath);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
                Instances data = FallDetectionClassifierFactory.getInstances(mContext);
                train(data, classifierPath);
        }
    }

    @Override
    public void train(Instances data, String classifierPath) {

        mKnn = new IBk();
        try {
            mKnn.setKNN(7);
            mKnn.setMeanSquared(true);
            LinearNNSearch search = new LinearNNSearch();
            search.setDistanceFunction(new ManhattanDistance());
            mKnn.setNearestNeighbourSearchAlgorithm(search);
            mKnn.setDistanceWeighting(new SelectedTag(IBk.WEIGHT_INVERSE, IBk.TAGS_WEIGHTING));
            mKnn.buildClassifier(data);
            if (classifierPath != null)
                SerializationHelper.write(classifierPath, mKnn);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void trainIncr(Instance data, String classifierPath) {

        try {
            mKnn.updateClassifier(data);
            if (classifierPath != null)
                SerializationHelper.write(classifierPath, mKnn);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public double run(double[] input) {
        double res[] = new double[]{0};
        Instances data = FallDetectionClassifierFactory.getInstances(mContext);
        Instance instance = new DenseInstance(data.numAttributes());
        instance.setDataset(data);

        for (int i=0; i<input.length; i++)
            instance.setValue(i, input[i]);

        try {
            res = mKnn.distributionForInstance(instance);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res[0];
    }

    @Override
    public String toString() {
        return "KnnClassifier Options: " + Arrays.toString(mKnn.getOptions());
    }
}

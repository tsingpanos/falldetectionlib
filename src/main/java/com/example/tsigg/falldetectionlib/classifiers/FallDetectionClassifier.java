package com.example.tsigg.falldetectionlib.classifiers;

import weka.core.Instances;

/**
 * Created by tsigg on 31/10/2016.
 */

public interface FallDetectionClassifier {
    void init();
    void train(Instances data, String classifierPath);
    double run(double[] input);
    String toString();
}

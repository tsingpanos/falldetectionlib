package com.example.tsigg.falldetectionlib.classifiers;

import android.content.Context;

import com.example.tsigg.falldetectionlib.LibUtility;

import java.io.File;
import java.util.Arrays;

import weka.classifiers.functions.MultilayerPerceptron;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.SerializationHelper;


/**
 * Created by tsigg on 31/10/2016.
 */

public class MlpClassifier implements FallDetectionClassifier {

    private static final String TAG = MlpClassifier.class.getSimpleName();
    private Context mContext;
    private String mPath;
    private MultilayerPerceptron mMlp;
    private static MlpClassifier mMLPInstance;

    public static MlpClassifier getMLPInstance(Context context) {
        if (mMLPInstance == null)
            mMLPInstance = new MlpClassifier(context);
        return mMLPInstance;
    }

    private MlpClassifier(Context context) {
        mContext = context.getApplicationContext();
        mPath = LibUtility.getExternalStorageDirectory(mContext);
        init();
    }

    @Override
    public void init() {
        String classifierPath = mPath + File.separator + LibUtility.FOLDER_CLASSIFIER + File.separator + LibUtility.FILE_MODEL_MLP;

        File file = new File(classifierPath);
        if (file.exists() && !file.isDirectory()) {
            try {
                mMlp = (MultilayerPerceptron) SerializationHelper.read(classifierPath);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Instances data = FallDetectionClassifierFactory.getInstances(mContext);
            train(data, classifierPath);
        }
    }

    @Override
    public double run(double[] input) {
        double res[] = new double[]{0};
        Instances data = FallDetectionClassifierFactory.getInstances(mContext);
        Instance instance = new DenseInstance(data.numAttributes());
        instance.setDataset(data);

        for (int i=0; i<input.length; i++)
            instance.setValue(i, input[i]);

        try {
            res = mMlp.distributionForInstance(instance);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res[0];
    }

    @Override
    public void train(Instances data, String classifierPath) {
        mMlp = new MultilayerPerceptron();
        try {
            mMlp.setMomentum(0.7);
            mMlp.setLearningRate(0.1);
            mMlp.setHiddenLayers("4");
            mMlp.setTrainingTime(500);
            mMlp.setNormalizeAttributes(true);

            mMlp.buildClassifier(data);
            if (classifierPath != null)
                SerializationHelper.write(classifierPath, mMlp);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return "MlpClassifier Options: " + Arrays.toString(mMlp.getOptions());
    }
}

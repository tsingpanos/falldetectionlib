/*
@startuml
package falldetection.classifiers {
    class KnnClassifier .le.|> Classifier
    class MlpClassifier
    class SvmClassifier
    class VoteClassifier
    class ClassifierFactory
    interface Classifier
}

class KnnClassifier {
    -mKnn : weka.classifiers.lazy.IBk
    -{static}mKNNInstance : KnnClassifier
    -KnnClassifier()
    +{static}getKNNInstance() : KnnClassifier
}

class MlpClassifier implements Classifier {
    -mMlp : weka.classifiers.functions.MultilayerPerceptron
    -{static}mMLPInstance : MlpClassifier
    -MlpClassifier()
    +{static}getMLPInstance() : MlpClassifier
}

class SvmClassifier implements Classifier {
    -mLibSVM : weka.classifiers.functions.LibSVM
    -{static} mLibSVMInstance : SvmClassifier
    -SvmClassifier()
    +{static} getSVMInstance() : SvmClassifier
}

class VoteClassifier implements Classifier {
    -mVote : weka.classifiers.meta.Vote
    -{static} mVoteInstance : VoteClassifier
    -VoteClassifier()
    +{static} getVoteInstance() : VoteClassifier
}

interface Classifier {
    +init()
    +run(Double[]) : Double
    +train(Instances, String)
    +toString() : String
}

class ClassifierFactory {
    -{static}  mInstances : Instances
    +ClassifierFactory()
    +getClassifier(String) : Classifier
    +{static}getInstances(Context) : Instances
    +{static}addInstance(Double[], String, Boolean) : Integer
    -{static}loadInstances(Context)
}
@enduml
*/

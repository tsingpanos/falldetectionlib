package com.example.tsigg.falldetectionlib.classifiers;

import android.content.Context;

import com.example.tsigg.falldetectionlib.LibUtility;

import java.io.File;
import java.util.Arrays;
import java.util.Vector;

import weka.classifiers.functions.LibSVM;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.SelectedTag;
import weka.core.SerializationHelper;

/**
 * Created by tsigg on 3/11/2016.
 */

public class SvmClassifier implements FallDetectionClassifier{
    private static final String TAG = SvmClassifier.class.getSimpleName();
    private Context mContext;
    private String mPath;
    private LibSVM mLibSVM;
    private static SvmClassifier mLibSVMInstance;

    public static SvmClassifier getSVMInstance(Context context) {
        if (mLibSVMInstance == null)
            mLibSVMInstance = new SvmClassifier(context);
        return mLibSVMInstance;
    }

    private SvmClassifier(Context context) {
        mContext = context.getApplicationContext();
        mPath = LibUtility.getExternalStorageDirectory(mContext);
        init();
    }

    @Override
    public void init() {
        String classifierPath = mPath + File.separator + LibUtility.FOLDER_CLASSIFIER + File.separator + LibUtility.FILE_MODEL_SVM;

        File file = new File(classifierPath);
        if (file.exists() && !file.isDirectory()) {
            try {
                mLibSVM = (LibSVM) SerializationHelper.read(classifierPath);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Instances data = FallDetectionClassifierFactory.getInstances(mContext);
            train(data, classifierPath);
        }
    }

    @Override
    public double run(double[] input) {
        double res[] = new double[]{0};
        Instances data = FallDetectionClassifierFactory.getInstances(mContext);
        Instance instance = new DenseInstance(data.numAttributes());
        instance.setDataset(data);

        for (int i=0; i<input.length; i++)
            instance.setValue(i, input[i]);

        try {
            res = mLibSVM.distributionForInstance(instance);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res[0];
    }

    @Override
    public void train(Instances data, String classifierPath) {
        mLibSVM = new LibSVM();
        try{
            mLibSVM.setSVMType(new SelectedTag(LibSVM.SVMTYPE_C_SVC,LibSVM.TAGS_SVMTYPE));
            mLibSVM.setNormalize(true);
            mLibSVM.setKernelType(new SelectedTag(LibSVM.KERNELTYPE_RBF, LibSVM.TAGS_KERNELTYPE));
            mLibSVM.setCost(9);
            mLibSVM.setCoef0(2);
            mLibSVM.setGamma(0.8);

            mLibSVM.buildClassifier(data);
            if (classifierPath != null)
                SerializationHelper.write(classifierPath, mLibSVM);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return "SvmClassifier Options: " + Arrays.toString(_getOptions());
    }

    private String[] _getOptions() {
        Vector result = new Vector();
        result.add("-S");
        result.add("" + mLibSVM.getSVMType());
        result.add("-K");
        result.add("" + mLibSVM.getKernelType());
        result.add("-D");
        result.add("" + mLibSVM.getDegree());
        result.add("-G");
        result.add("" + mLibSVM.getGamma());
        result.add("-R");
        result.add("" + mLibSVM.getCoef0());
        result.add("-N");
        result.add("" + mLibSVM.getNu());
        result.add("-M");
        result.add("" + mLibSVM.getCacheSize());
        result.add("-C");
        result.add("" + mLibSVM.getCost());
        result.add("-E");
        result.add("" + mLibSVM.getEps());
        result.add("-P");
        result.add("" + mLibSVM.getLoss());
        if(!mLibSVM.getShrinking()) {
            result.add("-H");
        }

        if(mLibSVM.getNormalize()) {
            result.add("-Z");
        }

        if(mLibSVM.getDoNotReplaceMissingValues()) {
            result.add("-V");
        }

        if(mLibSVM.getWeights().length() != 0) {
            result.add("-W");
            result.add("" + mLibSVM.getWeights());
        }

        if(mLibSVM.getProbabilityEstimates()) {
            result.add("-B");
        }

        return (String[])((String[])result.toArray(new String[result.size()]));
    }

}

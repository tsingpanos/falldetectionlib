package com.example.tsigg.falldetectionlib.classifiers;

import android.content.Context;

import com.example.tsigg.falldetectionlib.LibUtility;

import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.CSVLoader;
import weka.core.converters.CSVSaver;
import weka.filters.Filter;
import weka.filters.supervised.instance.ENNRule_Data_Editing;
import weka.filters.unsupervised.instance.EHC_Editing_through_Homogeneous_Clusters;

/**
 * Created by tsigg on 31/10/2016.
 */

public class FallDetectionClassifierFactory {

    private static final String TAG = FallDetectionClassifierFactory.class.getSimpleName();
    private static final int ATTR_NUM = 13;
    private Context mContext;
    private String mPath;
    private static Instances mInstances;
    private Logger mLogger;
    private static final int mClearDataTotal = 5;
    private static int mClearDataCounter = 0;

    public FallDetectionClassifierFactory(Context context) {
        mContext = context.getApplicationContext();
        mPath = LibUtility.getExternalStorageDirectory(mContext);
        mLogger = LibUtility.getFallDetectionLogger(context);
    }

    public FallDetectionClassifier getClassifier(String type) {
        if (type == null) {
            return null;
        }
        if (type.equalsIgnoreCase(LibUtility.TAG_CLASSIFIER_MLP)) {
            //Log.d(TAG, "MlpClassifier instance created");
            mLogger.fine("MlpClassifier instance created");
            return MlpClassifier.getMLPInstance(mContext);
        } else if (type.equalsIgnoreCase(LibUtility.TAG_CLASSIFIER_KNN)) {
            //Log.d(TAG, "KnnClassifier instance created");
            mLogger.fine("KnnClassifier instance created");
            return KnnClassifier.getKNNInstance(mContext);
        } else if (type.equalsIgnoreCase(LibUtility.TAG_CLASSIFIER_SVM)) {
            //Log.d(TAG, "SvmClassifier instance created");
            mLogger.fine("SvmClassifier instance created");
            return SvmClassifier.getSVMInstance(mContext);
        } else if (type.equalsIgnoreCase(LibUtility.TAG_CLASSIFIER_VOTE)) {
            //Log.d(TAG, "SvmClassifier instance created");
            mLogger.fine("SvmClassifier instance created");
            return SvmClassifier.getSVMInstance(mContext);
        }

        return null;
    }

    public static Instances getInstances(Context context) {
        if (mInstances == null) {
            loadInstances(context);
        }
        return mInstances;
    }

    private static void loadInstances(Context context) {
        String path = LibUtility.getExternalStorageDirectory(context);
        String trainingPath = path + File.separator + LibUtility.FOLDER_CLASSIFIER + File.separator + LibUtility.FILE_TRAIN_DATA;
        File file = new File(trainingPath);
        if (!file.exists()) {
            LibUtility.assetToFile(context, LibUtility.FILE_TRAIN_DATA, trainingPath);
        }

        try {
            CSVLoader loader = new CSVLoader();
            loader.setSource(new File(trainingPath));
            mInstances = loader.getDataSet();
            mInstances.setClassIndex(mInstances.numAttributes()-1);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static int addInstance(double[] newInstance, String clas, boolean save) {
        if (mInstances == null) {
            return 0;
        }

        Instance instance = new DenseInstance(mInstances.numAttributes());
        instance.setDataset(mInstances);

        for (int i=0; i<newInstance.length; i++)
            instance.setValue(i, newInstance[i]);

        instance.setClassValue(clas);

        mInstances.add(instance);

        mClearDataCounter ++;
        if (mClearDataCounter >= mClearDataTotal) {
            mClearDataCounter = 0;
            applyENNRuleFilter();

            CSVSaver saver = new CSVSaver();
            saver.setInstances(mInstances);
            try {
                saver.writeBatch();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return mInstances.size();
    }

    private static void applyENNRuleFilter() {
        ENNRule_Data_Editing ennRuleFilter = new ENNRule_Data_Editing();
        ennRuleFilter.setNumNeighbors(7);
        try {
            ennRuleFilter.setInputFormat(mInstances);
            mInstances = Filter.useFilter(mInstances, ennRuleFilter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void applyEHCRuleFilter() {
        EHC_Editing_through_Homogeneous_Clusters ehcRuleFilter = new EHC_Editing_through_Homogeneous_Clusters();
        try {
            ehcRuleFilter.setInputFormat(mInstances);
            mInstances = Filter.useFilter(mInstances, ehcRuleFilter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

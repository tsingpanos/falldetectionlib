package com.example.tsigg.falldetectionlib.classifiers;

import android.content.Context;

import com.example.tsigg.falldetectionlib.LibUtility;

import java.io.File;
import java.util.Arrays;

import weka.classifiers.meta.Vote;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.SerializationHelper;

/**
 * Created by tsigg on 3/1/2017.
 */

public class VoteClassifier implements FallDetectionClassifier {

    private Context mContext;
    private Vote mVote;
    private String mPath;
    private static VoteClassifier mVoteInstance;

    public static VoteClassifier getVoteInstance(Context context) {
        if (mVoteInstance == null)
            mVoteInstance = new VoteClassifier(context);
        return mVoteInstance;
    }

    public VoteClassifier(Context context) {
        mContext = context.getApplicationContext();
        mPath = LibUtility.getExternalStorageDirectory(mContext);
        init();
    }

    public void init() {
        String classifierPath = mPath + File.separator + LibUtility.FOLDER_CLASSIFIER + File.separator + LibUtility.FILE_MODEL_VOTE;

        File file = new File(classifierPath);
        if (file.exists() && !file.isDirectory()) {
            try {
                mVote = (Vote) SerializationHelper.read(classifierPath);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Instances data = FallDetectionClassifierFactory.getInstances(mContext);
            train(data, classifierPath);
        }
    }

    public void train(Instances data, String classifierPath) {
        mVote = new Vote();
        try {

            mVote.buildClassifier(data);
            if (classifierPath != null)
                SerializationHelper.write(classifierPath, mVote);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public double run(double[] input) {
        double res[] = new double[]{0};
        Instances data = FallDetectionClassifierFactory.getInstances(mContext);
        Instance instance = new DenseInstance(data.numAttributes());
        instance.setDataset(data);

        for (int i=0; i<input.length; i++)
            instance.setValue(i, input[i]);

        try {
            res = mVote.distributionForInstance(instance);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res[0];
    }

    public String toString() {
        return "VoteClassifier Options: " + Arrays.toString(mVote.getOptions());
    }
}
